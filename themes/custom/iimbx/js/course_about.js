(function ($, Drupal) {
    Drupal.behaviors.myfunction = {
        attach: function(context, settings) {

                $('a[rel*=leanModal]').leanModal({ top : 200, overlay : 0.4, closeButton: ".modal_close" });

                var cid = $("#course_id").val();
                var cname = $("#course_name").val();
                var edxpath = $("#edx_site_path").val();
                var enrollment_start_date = $("#enrollment_start").val();
                var enrollment_end_date =$("#enrollment_end").val();
                var is_inviteonly = false;
                var is_loggedin = false;
                var already_enrolled = false;
                var is_enrollment_on = false;

                enrollstartstamp = new Date(enrollment_start_date).getTime();
                enrollendstamp = new Date(enrollment_end_date).getTime();
                current_date = new Date().getTime();
                if(enrollstartstamp <= current_date && enrollendstamp >= current_date)          is_enrollment_on = true; 


	        var decodedCookie = decodeURIComponent(document.cookie);
        	var ca = decodedCookie.split(';');
	        var edxinfo= {};

        	for(var i = 0; i <ca.length; i++)
         	{
                	var c = ca[i];
	                while (c.charAt(0) == ' ') {
        	         c = c.substring(1);
        	        }
	
                	if(c.indexOf(edx_cookie_name) == 0) {
	                  var is_loggedin = c.substring(name.length, c.length);
        	        }
                }

           $.ajax({
           type: "GET",
	   url: edxpath+"/api/courses/v1/courses/"+cid,
	   cache: false,           
           success: function(data){	
            var image_link = data.media.course_image.uri;
            image_link = image_link.substr(0, image_link.lastIndexOf("@")+1)
            $("#about_data").html(data.overview.replace('/static/',edxpath+""+image_link));

            }
           });

          $.ajax({
           type: "GET",
           url: edxpath+"/api/enrollment/v1/course/"+cid,
           cache: false,
           async:false,
           success: function(data){
            if(data.invite_only){
                is_inviteonly = true;
             }
            }
           });

          $.ajax({
           type: "GET",
           url: edxpath+"/api/enrollment/v1/enrollment/"+cid,
           cache: false,
           async:false,
           xhrFields: { withCredentials: true },  
           success: function(data){
             if(data.is_active) already_enrolled =true;
          }
         }); 

         if(is_loggedin && already_enrolled){
              $('.main-cta').html('<a href="'+edxpath+'/courses/'+cid+'/info"><span class="register disabled">You are enrolled in this course</span><strong>View Course</strong></a>');
          }
         else if(is_inviteonly){ 
              $('.main-cta').html('<span class="disabled register">Enrollment in this course is by invitation only</span>');
          } 
         else if(is_enrollment_on){
             $('.main-cta').html('<a href="#" class="register ajaxCall">Enroll in '+cname+' </a><div id="register_error"/>');

          }
         else{
           $('.main-cta').html('<span class="disabled register">Enrollment date is over.</span>');
          }

    $(".ajaxCall").click(function(event) {
      $.ajax({
        type:'POST',
        url: edxpath+'/change_enrollment',
        data:$('#class_enroll_form').serialize(),
        success: function(responseData, textStatus, xhr) {
           if (xhr.responseText == "") {
	     // location.href = edxpath+"/dashboard";
           }
           else {
            //  location.href = xhr.responseText;
           }
        },
        error: function(xhr, textStatus, errorThrown) {
           if (xhr.status == 403) {
             location.href = edxpath+"/register?course_id="+cid+"&enrollment_action=enroll";
           } else {
             $('#register_error').html((xhr.responseText ? xhr.responseText : "An error occurred. Please try again later.")).css("display", "block");
           }
        }
      });
    });



/*

    $('#class_enroll_form').on('ajax:complete', function(event, xhr) {
      if(xhr.status == 200) {
        if (xhr.responseText == "") {
          location.href = edx_site_path+"/dashboard";
        }
        else {
          location.href = xhr.responseText;
        }
      } else if (xhr.status == 403) {
          location.href = edx_site_path+"/register?course_id="+cid+"&enrollment_action=enroll";
      } else {
        $('#register_error').html(
            (xhr.responseText ? xhr.responseText : "An error occurred. Please try again later.")
        ).css("display", "block");
      }
    });
       
*/

       }
    }
})(jQuery, Drupal);
