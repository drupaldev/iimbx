/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This JS file provides the function for accordian in FAQs and menus *
*          used in iimbx theme.                                          * 
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
* Date: 7-Feb-2017                                                            *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date     By             Description                                 *
* --------------------------------------------------------------------------- *
* 1.0     7-02-17  Mangesh G      Initial Version                             *
*                                                                             *
*                                                                             *
*******************************************************************************
*/

/**
* This function provides accordian used for FAQs. 
*/
(function($) {
        Drupal.behaviors.myBehavior = {
          attach: function (context, settings) {
/** Hide show function for FAQs page.
  * Initialy hide all answers.
  */
 
              $('.showfaqque').next().hide(); 

//on click event called on question click
               
               $('.showfaqque').unbind("click").click(function(){  

//if answer is already open

               if($(this).next().css('display') == 'block'){  
// change down arrow to up arrow and close the answer                
                $(this).removeClass('showfaqque-close').next().slideToggle("slow"); 
               }
// if answer is closed               
               else { 
// change down arrow to down arrow and open the answer              
                $(this).addClass('showfaqque-close').next().slideToggle("slow"); 
               }


           });
       $(".view-filters").css('display','none');
     
 

        var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(';');
        var edxinfo= {};
       

        for(var i = 0; i <ca.length; i++)
   	 {
        	var c = ca[i];
        	while (c.charAt(0) == ' ') {
         	 c = c.substring(1);
                }

        	if(c.indexOf(edx_cookie_name) == 0) {
        	  var is_loggedin = c.substring(name.length, c.length);
        	}

        	if(c.indexOf(user_cookie_name) == 0)
       		{
           	  var userinfo = c.substring(name.length, c.length);
                  var t1= userinfo.replace(user_cookie_name+'=',"");
                  var t3 = t1.replace(/\\054/g, ',');
                  var t4 = t3.replace(/\\"/g,'"');
                  var t5= t4.substring(1,t4.length-1);
                  edxinfo = JSON.parse(t5);
                }

         }



           var usernamex= edxinfo['username']; 
           $("#username").html(usernamex);
          
           if(is_loggedin) {
            $("#auth_header").css("display", "block");
            $("#guest_header").css("display", "none");
	    $("#register_menu").css("display", "none");
            $("#login_menu_item").css("display", "none");
	    $("#after_login_menu").css("display", "block");
           }
           else
           {
            $("#auth_header").css("display", "none");
            $("#guest_header").css("display", "block");
	    $("#after_login_menu").css("display", "none");

           }
          if(edxinfo['header_urls']){
          var profile_link = edxinfo['header_urls']['learner_profile'];
          var logout_link = edxinfo['header_urls']['logout'];
          var account_link = edxinfo['header_urls']['account_settings'];
          }
                                                                                         
           $("#profile_link").attr("href", profile_link);
	   $("#logout_link").attr("href", logout_link);
           $("#account_link").attr("href", account_link);
              

           }
          }

          

// responsive main menu: drop down toggle

          $('#myResponsiveMenuButton').click(function() {           
                      
             if($(".nav-global").hasClass( "responsive" )) {
                $(".nav-global").removeClass( "responsive" );               
             }
             else{
                $(".nav-global").addClass( "responsive" );                                   
                $(".tempclass").css( "display","block");
             }
          });

          $('body').click(function() {           
                      
             if($(".nav-global").hasClass( "responsive" ))
                $(".nav-global").removeClass( "responsive" );               
            
          });



/**
* To make dropdown actually work
* To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
*/
          $(".sidebar select").change(function() {
            window.location = $(this).find("option:selected").val();
          });


      $( ".filtermenubutton" ).click(function() {

            var tempstyle = $( "#navbarCollapsec" ).css('display');

             $( "#navbarCollapsec" ).slideToggle( "slow", function() {
              // Animation complete.
            });

            if(tempstyle.trim() == 'none') $( "#navbarCollapsec" ).css('display','block');
            else $( "#navbarCollapsec" ).css('display','none');
          });      
     $("#test123").click(function (){
      $(".dropdown-menu").slideToggle("slow",function(){});
});




$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}
$("#views-exposed-form-course-search-results-page-1").css('display','none');


       if($.urlParam('search_query')!=0)
            {
                var keyword = $("#edit-search-query").val();
                $('input[name=search_query]').val(keyword);
                 
            }


})(jQuery);

