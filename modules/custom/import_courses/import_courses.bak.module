<?php
/*
# This file is part of IIMBX-Drupal.
#
# IIMBX-Drupal is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License as published by the Free 
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# IIMBX-Drupal is distributed in the hope that it will be useful,but 
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
# more details.
#
# You should have received a copy of the GNU General Public License along with
# IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################
#                                                                             #
# Purpose: It contains logic and main program of module.                      #
#                                                                             #
# Created by: Mangesh Gharate                                                 #
#                                                                             #
# Date: 19-jul-2017                                                            #
#                                                                             #
#                                                                             #
# Change Log:                                                                 #
# Version Date     By             Description                                 #
# --------------------------------------------------------------------------- #
# 1.0     19-07-17  Mangesh G      Initial Version                             #

###############################################################################
*/
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use \Drupal\file\Entity\File;
use Drupal\Component\Serialization\Yaml;

/* function to get the YML file */
   function get_config_filepath() 
   {
       return getcwd()."/".drupal_get_path('module','import_courses')."/config.yml";
   }

/* Function to Fetch data through API and decode it in json format */
   function get_json_dump($url)
   {
         $response = \Drupal::httpClient()->get($url, array('headers' => array('Accept' => 'application/json')));
         $res_status = $response->getStatusCode();
         $response = $response->getBody()->getContents();
         $result = json_decode($response,true);
         $result_array[0] = $res_status;
         $result_array[1] = $result;
         
         return $result_array;
   }

/*This function is used to fetch data from nested array e.g Image_url data but here we are using it to fetch all values */
   function deriveValue($CourseInfo,$TreeList)
   {
          $tmp=$CourseInfo;
          $TreePath=explode(',',$TreeList);
           foreach($TreePath as $indx)
           {
              $tmp=$tmp[$indx];
           }
          return $tmp;
   }



/** Implements hook_cron() **/
   function import_courses_cron() 
   {
      global $node_log_str;
      global $node_report_str;
      global $node_url_array;
   
      $api_endpath = \Drupal::config('node.settings')->get('edx_site_path');

      // get the contents of config file and dump the yml contents in map_array
      $config_file_content = file_get_contents(get_config_filepath());

      $map_array = Yaml::decode($config_file_content);
      $result_arr = get_json_dump($api_endpath."/api/courses/v1/courses/");
      
      if($result_arr[0] == 200){
           $result =$result_arr[1];
      }
      else{	
	   return false;
      }


       /* It will give the count of pages on which the course data is present */
 
       $page= $result[$map_array['pagination']];
       $page_count= $page['num_pages'];
      
       /* loop to fetch the data from all available pages */

       for($count =1;$count<=$page_count;$count++)
       {
      //   $config_file_content = file_get_contents( get_config_filepath());
      //   $map_array = Yaml::decode($config_file_content);
         
         $result_arr=get_json_dump($api_endpath."/api/courses/v1/courses/?page=".$count);
	 if($result_arr[0] == 200){
           $result =$result_arr[1];
         }
	 else
	 {
	   return false;
	 }
	
         foreach ($result[$map_array['course_list']] as $course) 
            {
              $course_code = deriveValue($course,$map_array['course_code']);
              $course_url = deriveValue($course,$map_array['link_url']);
              $nids = \Drupal::entityQuery('node')     
	      ->condition('field_course_link', $course_url)
	      ->condition('type', 'course')    
	      ->range(0, 1)        
	      ->execute();
              $nodeCount = count($nids);        
              $node_url_array[] = $course_url;    
	
        
              $current_date = strtotime(date("Y-m-d"));
              $start_date = date("Y-m-d",strtotime(deriveValue($course,$map_array['start_date'])));
              $end_date = date("Y-m-d", strtotime(deriveValue($course,$map_array['end_date'])));       
              $start_date_stamp = strtotime(deriveValue($course,$map_array['start_date']));
              $end_date_stamp = strtotime(deriveValue($course,$map_array['end_date']));
              $start_type = strtotime(deriveValue($course,$map_array['start_type']));
 


             //found course should published or not
        
              $status=1;
              $announce_date=date("Y-m-d",strtotime(deriveValue($course,$map_array['announcement'])));

              if ($announce_date) 
               {
                   $announcement_date =strtotime($announce_date);
                      if( $current_date >= $announcement_date)
                         $status = 1;
                      else
                         $status = 0;
               }

              $course_list_var  = deriveValue($course,$map_array['course_list']);
              $organisation_var = deriveValue($course,$map_array['organisation']);
              $course_code_var  = deriveValue($course,$map_array['course_code']);
              $link_url_var     = deriveValue($course,$map_array['link_url']);
              $image_url_var    = deriveValue($course,$map_array['image_url']);
              $course_name_var  = deriveValue($course, $map_array['course_name']);
              $display_string   = deriveValue($course, $map_array['announcement']);
              $enrollment_start   = deriveValue($course, $map_array['enrollment_start']);
              $enrollment_end = deriveValue($course, $map_array['enrollment_end']);
              $public_info   = deriveValue($course, $map_array['public_info']); 
  
     
              if(validate_course($course,$map_array))
              {
               try
                {
                 if($nodeCount==0)
                   {

                      // If course NOT exist create new course (content)

                      $node = Node::create
                           ([
                           'type' => 'course',
                          'title' => $link_url_var,
                         'status' => $status,
                        'promote' => 0,
                        'comment' => 1,
                            'uid' => 1,
              'field_course_code' => $course_code_var,              
              'field_course_link' => $link_url_var,
              'field_course_name' => $course_name_var,          
             'field_course_image' => $image_url_var,
             'field_organisation' => $organisation_var,
             'field_course_start' => $start_date,
               'field_course_end' => $end_date,
      'field_course_announcement' => $announce_date,             
       'field_display_time_stamp' => $start_date_stamp,
           'field_display_string' => $display_string,
           'field_display_string' => $enrollment_start,
		'field_display_string' => $enrollment_end,
		'field_display_string' => $public_info
 
                           ]);

                      $node->save();
                      $course_added++;
                }  
             else
                {
                      $current_node_id = current($nids);
                  
                  // If course already exist update (content)
                 
                      $node = Node::load($current_node_id);
                      $node->status = $status;  
                      $node->title = $link_url_var;               
                      $node->field_course_code= $course_code_var;              
                      $node->field_course_link= $link_url_var;
                      $node->field_course_name= $course_name_var;          
                      $node->field_course_image= $image_url_var;
                      $node->field_organisation= $organisation_var; 
                      $node->field_course_start= $start_date;
                      $node->field_course_end= $end_date;
                      $node->field_course_announcement= $announce_date;             
                      $node->field_display_time_stamp= $start_date_stamp;
                      $node->field_display_string = $display_string;
                      $node->field_display_string = $enrollment_start;
                      $node->field_display_string = $enrollment_end;
                      $node->field_display_string = $public_info;

                      $node->save();
                      $course_updated++;
               }          
            }
        
            catch (exception $e) 
          
            {
                 // update_log($e,'error');
                 $node_log_str .="  Error: ".$e."\n";
            }
        } 
      }      	
    }
  }

  //Courses removed from openEdx will unpublished from drupal by this function
  
      function unpublish_courses() 
      { 
            global $node_url_array;
            global $node_report_str;
   
            $course_unpublished = 0;   

            $available_nids = \Drupal::entityQuery('node')       
                            ->condition('type', 'course')             
                            ->execute();

            foreach ($available_nids as $available_nid)
             { 
                $nodex = Node::load($available_nid);
                $node_link_url = $nodex->field_course_link->value;
        
             if(!in_array($node_link_url, $node_url_array))
              {
                 $nodex->status = 0;
                 $nodex->save();
                 $course_unpublished++;
              }
            }
      }

      // Course data validation function
      function validate_course($course,$map_array)
      {
         return true;
      }

