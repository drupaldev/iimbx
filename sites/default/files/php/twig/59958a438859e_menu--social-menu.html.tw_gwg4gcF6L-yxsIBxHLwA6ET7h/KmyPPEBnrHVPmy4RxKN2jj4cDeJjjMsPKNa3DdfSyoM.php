<?php

/* themes/custom/iimbx/templates/menu--social-menu.html.twig */
class __TwigTemplate_e23ce830968677616b09553bb6daf7024b4d4f0c259dd5fd08ff41588a2255be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6e02ec5e96284d9ff02480dbe61ac0d71ca78814a52a44660c88c4744b59261c = $this->env->getExtension("native_profiler");
        $__internal_6e02ec5e96284d9ff02480dbe61ac0d71ca78814a52a44660c88c4744b59261c->enter($__internal_6e02ec5e96284d9ff02480dbe61ac0d71ca78814a52a44660c88c4744b59261c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/menu--social-menu.html.twig"));

        $tags = array("import" => 55, "macro" => 63, "if" => 65, "for" => 68);
        $filters = array();
        $functions = array("link" => 71);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('import', 'macro', 'if', 'for'),
                array(),
                array('link')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 55
        $context["menus"] = $this;
        // line 56
        echo "
";
        // line 61
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($context["menus"]->getmenu_links((isset($context["items"]) ? $context["items"] : null), (isset($context["attributes"]) ? $context["attributes"] : null), 0)));
        echo "

";
        // line 78
        echo "
";
        
        $__internal_6e02ec5e96284d9ff02480dbe61ac0d71ca78814a52a44660c88c4744b59261c->leave($__internal_6e02ec5e96284d9ff02480dbe61ac0d71ca78814a52a44660c88c4744b59261c_prof);

    }

    // line 63
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            $__internal_cb23bbe3d611f443d081b8f55db5a41298606112ac47f460d050cd716fed3834 = $this->env->getExtension("native_profiler");
            $__internal_cb23bbe3d611f443d081b8f55db5a41298606112ac47f460d050cd716fed3834->enter($__internal_cb23bbe3d611f443d081b8f55db5a41298606112ac47f460d050cd716fed3834_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "menu_links"));

            // line 64
            echo "  ";
            $context["menus"] = $this;
            // line 65
            echo "  ";
            if ((isset($context["items"]) ? $context["items"] : null)) {
                // line 66
                echo "    <nav class=\"nav-social\">
     <ul>
      ";
                // line 68
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 69
                    echo "      
      <li class=\"nav-social-02\">
        ";
                    // line 71
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->env->getExtension('drupal_core')->getLink($this->getAttribute($context["item"], "title", array()), $this->getAttribute($context["item"], "url", array())), "html", null, true));
                    echo "
      </li>
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 74
                echo "    </ul>
    </nav>
  ";
            }
            
            $__internal_cb23bbe3d611f443d081b8f55db5a41298606112ac47f460d050cd716fed3834->leave($__internal_cb23bbe3d611f443d081b8f55db5a41298606112ac47f460d050cd716fed3834_prof);

        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/menu--social-menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 74,  99 => 71,  95 => 69,  91 => 68,  87 => 66,  84 => 65,  81 => 64,  64 => 63,  56 => 78,  51 => 61,  48 => 56,  46 => 55,);
    }

    public function getSource()
    {
        return "{#
/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file is created for the display block for system menu.        *
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
* Date: 19-JUL-2017                                                           *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date      By             Description                                *
* --------------------------------------------------------------------------- *
* 1.0     20-07-17  Mangesh G      Initial Version                            *
*                                                                             *
*                                                                             *
*******************************************************************************
 */

/**
 * @file
 * Theme override to display a menu.
 *
 * Available variables:
 * - menu_name: The machine name of the menu.
 * - items: A nested list of menu items. Each menu item contains:
 *   - attributes: HTML attributes for the menu item.
 *   - below: The menu item child items.
 *   - title: The menu link title.
 *   - url: The menu link url, instance of \\Drupal\\Core\\Url
 *   - localized_options: Menu link localized options.
 *   - is_expanded: TRUE if the link has visible children within the current
 *     menu tree.
 *   - is_collapsed: TRUE if the link has children within the current menu tree
 *     that are not currently visible.
 *   - in_active_trail: TRUE if the link is in the active trail.
 */
#}
{% import _self as menus %}

{#
  We call a macro which calls itself to render the full tree.
  @see http://twig.sensiolabs.org/doc/tags/macro.html
#}
{{ menus.menu_links(items, attributes, 0) }}

{% macro menu_links(items, attributes, menu_level) %}
  {% import _self as menus %}
  {% if items %}
    <nav class=\"nav-social\">
     <ul>
      {% for item in items %}
      
      <li class=\"nav-social-02\">
        {{ link(item.title, item.url) }}
      </li>
    {% endfor %}
    </ul>
    </nav>
  {% endif %}
{% endmacro %}

";
    }
}
