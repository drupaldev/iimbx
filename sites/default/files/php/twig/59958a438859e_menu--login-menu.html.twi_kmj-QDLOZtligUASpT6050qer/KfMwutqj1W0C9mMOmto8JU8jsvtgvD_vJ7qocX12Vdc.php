<?php

/* themes/custom/iimbx/templates/menu--login-menu.html.twig */
class __TwigTemplate_9b4cbe7613fdae325f2dbd79aae5f8f82601f591773f8a97a195975bb556ae99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_227fbc06a4bf166f90ee97871c615a1b4589c2e62a50bba98dccc714b19f431e = $this->env->getExtension("native_profiler");
        $__internal_227fbc06a4bf166f90ee97871c615a1b4589c2e62a50bba98dccc714b19f431e->enter($__internal_227fbc06a4bf166f90ee97871c615a1b4589c2e62a50bba98dccc714b19f431e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/menu--login-menu.html.twig"));

        $tags = array("import" => 55, "macro" => 64, "if" => 66, "for" => 69);
        $filters = array();
        $functions = array("url" => 106, "link" => 72);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('import', 'macro', 'if', 'for'),
                array(),
                array('url', 'link')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 55
        $context["menus"] = $this;
        // line 56
        echo "
";
        // line 61
        echo "
";
        // line 62
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($context["menus"]->getmenu_links((isset($context["items"]) ? $context["items"] : null), (isset($context["attributes"]) ? $context["attributes"] : null), 0, (isset($context["edx_site_path"]) ? $context["edx_site_path"] : null), (isset($context["logged_username"]) ? $context["logged_username"] : null), (isset($context["logged_email"]) ? $context["logged_email"] : null), (isset($context["profile_link"]) ? $context["profile_link"] : null), (isset($context["logout_link"]) ? $context["logout_link"] : null), (isset($context["account_link"]) ? $context["account_link"] : null), (isset($context["is_sidebar"]) ? $context["is_sidebar"] : null))));
        echo "

";
        // line 101
        echo "
<ol class=\"user\" id=\"after_login_menu\" style=\"display:none;\">
      <li class=\"primary\">
         <a class=\"user-link\" href=\"";
        // line 104
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["edx_site_path"]) ? $context["edx_site_path"] : null), "html", null, true));
        echo "/dashboard\">
          <span class=\"sr\">Dashboard for:</span>
\t  <div style=\"float:left;\"><img class=\"user-image-frame\" src=\"";
        // line 106
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getUrl("<front>")));
        echo "themes/custom/iimbx/images/default_50.png\">
</div>
          <div id='username' style=\"float:left;margin:10px\"> ";
        // line 108
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["logged_username"]) ? $context["logged_username"] : null), "html", null, true));
        echo "
          </div>
          </a>
      </li>
      <li class=\"primary\">
        <button class=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\" id=\"test123\"><span class=\"sr\">More options dropdown</span><span class=\"fa fa-sort-desc\" aria-hidden=\"true\"></span></button>
       <ul class='dropdown-menu' aria-label='More Options' role='menu'>
\t<li><a href=\"";
        // line 115
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["edx_site_path"]) ? $context["edx_site_path"] : null), "html", null, true));
        echo "/dashboard\">Dashboard</a></li>
          <li><a href=";
        // line 116
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["account_link"]) ? $context["account_link"] : null), "html", null, true));
        echo " id=\"account_link\">Account Settings</a></li>
 <li><a href=\"";
        // line 117
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["profile_link"]) ? $context["profile_link"] : null), "html", null, true));
        echo "\" id=\"profile_link\">My Profile</a></li>
 <li><a href=\"";
        // line 118
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["logout_link"]) ? $context["logout_link"] : null), "html", null, true));
        echo "\" role='menuitem' id=\"logout_link\">Sign Out</a></li>

       </ul>
           </li>
    </ol>
";
        
        $__internal_227fbc06a4bf166f90ee97871c615a1b4589c2e62a50bba98dccc714b19f431e->leave($__internal_227fbc06a4bf166f90ee97871c615a1b4589c2e62a50bba98dccc714b19f431e_prof);

    }

    // line 64
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, $__edx_site_path__ = null, $__logged_username__ = null, $__logged_email__ = null, $__profile_link__ = null, $__logout_link__ = null, $__account_link__ = null, $__is_sidebar__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "edx_site_path" => $__edx_site_path__,
            "logged_username" => $__logged_username__,
            "logged_email" => $__logged_email__,
            "profile_link" => $__profile_link__,
            "logout_link" => $__logout_link__,
            "account_link" => $__account_link__,
            "is_sidebar" => $__is_sidebar__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            $__internal_2467ccef933d4196a231c625ba8ad720891a1e60640b2147787a5ce5e0ef330f = $this->env->getExtension("native_profiler");
            $__internal_2467ccef933d4196a231c625ba8ad720891a1e60640b2147787a5ce5e0ef330f->enter($__internal_2467ccef933d4196a231c625ba8ad720891a1e60640b2147787a5ce5e0ef330f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "menu_links"));

            // line 65
            echo "  ";
            $context["menus"] = $this;
            // line 66
            echo "  ";
            if ((isset($context["items"]) ? $context["items"] : null)) {
                // line 67
                echo "
      <ol class=\"left nav-global \">
    ";
                // line 69
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 70
                    echo "      <li class=\"nav-global-01 ";
                    if (((isset($context["is_sidebar"]) ? $context["is_sidebar"] : null) && ($this->getAttribute($context["item"], "title", array()) == "Programmes"))) {
                        echo "  is_programme ";
                    }
                    echo "\">

        ";
                    // line 72
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->env->getExtension('drupal_core')->getLink($this->getAttribute($context["item"], "title", array()), $this->getAttribute($context["item"], "url", array())), "html", null, true));
                    echo "
        ";
                    // line 73
                    if ($this->getAttribute($context["item"], "below", array())) {
                        // line 74
                        echo "          ";
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", array()), (isset($context["attributes"]) ? $context["attributes"] : null), ((isset($context["menu_level"]) ? $context["menu_level"] : null) + 1))));
                        echo "
        ";
                    }
                    // line 76
                    echo "\t
\t";
                    // line 77
                    if (($this->getAttribute($context["item"], "title", array()) == "Courses")) {
                        // line 78
                        echo "             <li class=\"list-inline nav-global-04\">
                 <div class=\"title\">
                      <div class=\"course-search\">


                          <form method=\"get\" action=\"";
                        // line 83
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getUrl("<front>")));
                        echo "/courses\">

                     <input class=\"search-input\" name=\"search_query\" id=\"header_search_query\" type=\"text\" placeholder=\"Search for a course\"></input>
                                 <button class=\"search-button\" type=\"submit\" id=\"search_course\">
                                 <i class=\"icon fa fa-search\" aria-hidden=\"true\"></i>
                                 </button>
                           </form>
                       </div>
                 </div>
            </li>
       ";
                    }
                    // line 94
                    echo "
      </li>
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 97
                echo "    </ol>
    
  ";
            }
            
            $__internal_2467ccef933d4196a231c625ba8ad720891a1e60640b2147787a5ce5e0ef330f->leave($__internal_2467ccef933d4196a231c625ba8ad720891a1e60640b2147787a5ce5e0ef330f_prof);

        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/menu--login-menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 97,  193 => 94,  179 => 83,  172 => 78,  170 => 77,  167 => 76,  161 => 74,  159 => 73,  155 => 72,  147 => 70,  143 => 69,  139 => 67,  136 => 66,  133 => 65,  109 => 64,  96 => 118,  92 => 117,  88 => 116,  84 => 115,  74 => 108,  69 => 106,  64 => 104,  59 => 101,  54 => 62,  51 => 61,  48 => 56,  46 => 55,);
    }

    public function getSource()
    {
        return "{#
/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file is created for the display block for system menu.        *
*                                                                             *
* Created by: VARUN M                                                         *
*                                                                             *
* Date: 20-JUL-2017                                                           *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date     By             Description                                 *
* --------------------------------------------------------------------------- *
* 1.0     20-07-17  Mangesh G      Initial Version                            *
*                                                                             *
*                                                                             *
*******************************************************************************


/**
 * @file
 * Theme override to display a menu.
 *
 * Available variables:
 * - menu_name: The machine name of the menu.
 * - items: A nested list of menu items. Each menu item contains:
 *   - attributes: HTML attributes for the menu item.
 *   - below: The menu item child items.
 *   - title: The menu link title.
 *   - url: The menu link url, instance of \\Drupal\\Core\\Url
 *   - localized_options: Menu link localized options.
 *   - is_expanded: TRUE if the link has visible children within the current
 *     menu tree.
 *   - is_collapsed: TRUE if the link has children within the current menu tree
 *     that are not currently visible.
 *   - in_active_trail: TRUE if the link is in the active trail.
 */
#}
{% import _self as menus %}

{#
  We call a macro which calls itself to render the full tree.
  @see http://twig.sensiolabs.org/doc/tags/macro.html
#}

{{ menus.menu_links(items, attributes, 0, edx_site_path, logged_username, logged_email, profile_link, logout_link, account_link, is_sidebar) }}

{% macro menu_links(items, attributes, menu_level, edx_site_path, logged_username, logged_email, profile_link, logout_link, account_link, is_sidebar) %}
  {% import _self as menus %}
  {% if items %}

      <ol class=\"left nav-global \">
    {% for item in items %}
      <li class=\"nav-global-01 {% if(is_sidebar and item.title == \"Programmes\") %}  is_programme {% endif %}\">

        {{ link(item.title, item.url) }}
        {% if item.below %}
          {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
        {% endif %}
\t
\t{% if item.title == \"Courses\" %}
             <li class=\"list-inline nav-global-04\">
                 <div class=\"title\">
                      <div class=\"course-search\">


                          <form method=\"get\" action=\"{{ url('<front>') }}/courses\">

                     <input class=\"search-input\" name=\"search_query\" id=\"header_search_query\" type=\"text\" placeholder=\"Search for a course\"></input>
                                 <button class=\"search-button\" type=\"submit\" id=\"search_course\">
                                 <i class=\"icon fa fa-search\" aria-hidden=\"true\"></i>
                                 </button>
                           </form>
                       </div>
                 </div>
            </li>
       {% endif %}

      </li>
    {% endfor %}
    </ol>
    
  {% endif %}
{% endmacro %}

<ol class=\"user\" id=\"after_login_menu\" style=\"display:none;\">
      <li class=\"primary\">
         <a class=\"user-link\" href=\"{{ edx_site_path }}/dashboard\">
          <span class=\"sr\">Dashboard for:</span>
\t  <div style=\"float:left;\"><img class=\"user-image-frame\" src=\"{{ url('<front>') }}themes/custom/iimbx/images/default_50.png\">
</div>
          <div id='username' style=\"float:left;margin:10px\"> {{ logged_username }}
          </div>
          </a>
      </li>
      <li class=\"primary\">
        <button class=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\" id=\"test123\"><span class=\"sr\">More options dropdown</span><span class=\"fa fa-sort-desc\" aria-hidden=\"true\"></span></button>
       <ul class='dropdown-menu' aria-label='More Options' role='menu'>
\t<li><a href=\"{{ edx_site_path }}/dashboard\">Dashboard</a></li>
          <li><a href={{ account_link }} id=\"account_link\">Account Settings</a></li>
 <li><a href=\"{{ profile_link }}\" id=\"profile_link\">My Profile</a></li>
 <li><a href=\"{{ logout_link }}\" role='menuitem' id=\"logout_link\">Sign Out</a></li>

       </ul>
           </li>
    </ol>
";
    }
}
