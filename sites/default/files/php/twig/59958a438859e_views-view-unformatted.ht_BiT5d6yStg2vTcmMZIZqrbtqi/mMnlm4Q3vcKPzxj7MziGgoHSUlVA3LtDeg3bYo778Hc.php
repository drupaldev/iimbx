<?php

/* themes/custom/iimbx/templates/views-view-unformatted.html.twig */
class __TwigTemplate_fd434aef323d9c4943d4a4e94bf43c390fe430d014423b0ff1d2c8577d0eedbf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a8eaf894aa5f7f1ba047259b20389384a1497b6c2960dcd115fefcb07924900 = $this->env->getExtension("native_profiler");
        $__internal_1a8eaf894aa5f7f1ba047259b20389384a1497b6c2960dcd115fefcb07924900->enter($__internal_1a8eaf894aa5f7f1ba047259b20389384a1497b6c2960dcd115fefcb07924900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/views-view-unformatted.html.twig"));

        $tags = array("for" => 19);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('for'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 18
        echo "<ul>
";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["rows"]) ? $context["rows"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
            // line 20
            echo "
    ";
            // line 21
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($context["row"], "content", array()), "html", null, true));
            echo "

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "</ul>
";
        
        $__internal_1a8eaf894aa5f7f1ba047259b20389384a1497b6c2960dcd115fefcb07924900->leave($__internal_1a8eaf894aa5f7f1ba047259b20389384a1497b6c2960dcd115fefcb07924900_prof);

    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/views-view-unformatted.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 24,  56 => 21,  53 => 20,  49 => 19,  46 => 18,);
    }

    public function getSource()
    {
        return "{#
/**
 * @file
 * Theme override to display a view of unformatted rows.
 *
 * Available variables:
 * - title: The title of this group of rows. May be empty.
 * - rows: A list of the view's row items.
 *   - attributes: The row's HTML attributes.
 *   - content: The row's content.
 * - view: The view object.
 * - default_row_class: A flag indicating whether default classes should be
 *   used on rows.
 *
 * @see template_preprocess_views_view_unformatted()
 */
#}
<ul>
{% for row in rows %}

    {{ row.content }}

{% endfor %}
</ul>
";
    }
}
