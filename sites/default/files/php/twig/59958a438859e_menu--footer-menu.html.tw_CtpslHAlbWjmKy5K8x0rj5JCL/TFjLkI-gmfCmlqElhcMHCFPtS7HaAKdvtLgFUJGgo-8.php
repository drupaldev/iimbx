<?php

/* themes/custom/iimbx/templates/menu--footer-menu.html.twig */
class __TwigTemplate_94cedf390195c956dbdfc32acada7aefa70b0263d56c6849e4165c9dbe55beb2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b20fde4a2f02ff95f8920ce9b1c778408d79a1035b6c5b58f722a63ed250ca7 = $this->env->getExtension("native_profiler");
        $__internal_4b20fde4a2f02ff95f8920ce9b1c778408d79a1035b6c5b58f722a63ed250ca7->enter($__internal_4b20fde4a2f02ff95f8920ce9b1c778408d79a1035b6c5b58f722a63ed250ca7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/menu--footer-menu.html.twig"));

        $tags = array("import" => 55, "macro" => 63, "if" => 65, "for" => 69);
        $filters = array();
        $functions = array("link" => 71);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('import', 'macro', 'if', 'for'),
                array(),
                array('link')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 55
        $context["menus"] = $this;
        // line 56
        echo "
";
        // line 61
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($context["menus"]->getmenu_links((isset($context["items"]) ? $context["items"] : null), (isset($context["attributes"]) ? $context["attributes"] : null), 0)));
        echo "

";
        
        $__internal_4b20fde4a2f02ff95f8920ce9b1c778408d79a1035b6c5b58f722a63ed250ca7->leave($__internal_4b20fde4a2f02ff95f8920ce9b1c778408d79a1035b6c5b58f722a63ed250ca7_prof);

    }

    // line 63
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            $__internal_7ce49dcfc1da92c5493cbf863b0465e089d26f03eaef071f3e74fa44c365e31d = $this->env->getExtension("native_profiler");
            $__internal_7ce49dcfc1da92c5493cbf863b0465e089d26f03eaef071f3e74fa44c365e31d->enter($__internal_7ce49dcfc1da92c5493cbf863b0465e089d26f03eaef071f3e74fa44c365e31d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "menu_links"));

            // line 64
            echo "  ";
            $context["menus"] = $this;
            // line 65
            echo "  ";
            if ((isset($context["items"]) ? $context["items"] : null)) {
                // line 66
                echo "
      <ol>

    ";
                // line 69
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["items"]) ? $context["items"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 70
                    echo "      <li class=\"nav-colophon-01\">
        ";
                    // line 71
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->env->getExtension('drupal_core')->getLink($this->getAttribute($context["item"], "title", array()), $this->getAttribute($context["item"], "url", array())), "html", null, true));
                    echo "
        ";
                    // line 72
                    if ($this->getAttribute($context["item"], "below", array())) {
                        // line 73
                        echo "          ";
                        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", array()), (isset($context["attributes"]) ? $context["attributes"] : null), ((isset($context["menu_level"]) ? $context["menu_level"] : null) + 1))));
                        echo "
        ";
                    }
                    // line 75
                    echo "      </li>
      
    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 78
                echo "    </ol>
  ";
            }
            
            $__internal_7ce49dcfc1da92c5493cbf863b0465e089d26f03eaef071f3e74fa44c365e31d->leave($__internal_7ce49dcfc1da92c5493cbf863b0465e089d26f03eaef071f3e74fa44c365e31d_prof);

        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/menu--footer-menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 78,  108 => 75,  102 => 73,  100 => 72,  96 => 71,  93 => 70,  89 => 69,  84 => 66,  81 => 65,  78 => 64,  61 => 63,  51 => 61,  48 => 56,  46 => 55,);
    }

    public function getSource()
    {
        return "{#
/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file is created for the display menu appear in footer.        *
*                                                                             *
* Created by: Varun Madkaikar                                                 *
*                                                                             *
* Date: 21-07-2017                                                            *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date      By               Description                              *
* --------------------------------------------------------------------------- *
* 1.0     21-07-17  Varun Madkaikar   Initial Version                         *
*                                                                             *
*                                                                             *
*******************************************************************************
 */

/**
 * @file
 * Theme override to display a menu.
 *
 * Available variables:
 * - menu_name: The machine name of the menu.
 * - items: A nested list of menu items. Each menu item contains:
 *   - attributes: HTML attributes for the menu item.
 *   - below: The menu item child items.
 *   - title: The menu link title.
 *   - url: The menu link url, instance of \\Drupal\\Core\\Url
 *   - localized_options: Menu link localized options.
 *   - is_expanded: TRUE if the link has visible children within the current
 *     menu tree.
 *   - is_collapsed: TRUE if the link has children within the current menu tree
 *     that are not currently visible.
 *   - in_active_trail: TRUE if the link is in the active trail.
 */
#}
{% import _self as menus %}

{#
  We call a macro which calls itself to render the full tree.
  @see http://twig.sensiolabs.org/doc/tags/macro.html
#}
{{ menus.menu_links(items, attributes, 0) }}

{% macro menu_links(items, attributes, menu_level) %}
  {% import _self as menus %}
  {% if items %}

      <ol>

    {% for item in items %}
      <li class=\"nav-colophon-01\">
        {{ link(item.title, item.url) }}
        {% if item.below %}
          {{ menus.menu_links(item.below, attributes, menu_level + 1) }}
        {% endif %}
      </li>
      
    {% endfor %}
    </ol>
  {% endif %}
{% endmacro %}
";
    }
}
