<?php

/* themes/custom/iimbx/templates/views-view-fields--header_logo.html.twig */
class __TwigTemplate_2af46dc00a1caf48564754e0095983cc847ad40583fb1fa1bc9a52de8a66a954 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_381d5a4707bd87ca9e74655b4c4ab7f2cb269d8e10f7d6cc5886414a41d55436 = $this->env->getExtension("native_profiler");
        $__internal_381d5a4707bd87ca9e74655b4c4ab7f2cb269d8e10f7d6cc5886414a41d55436->enter($__internal_381d5a4707bd87ca9e74655b4c4ab7f2cb269d8e10f7d6cc5886414a41d55436_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/views-view-fields--header_logo.html.twig"));

        $tags = array("set" => 38, "for" => 40, "if" => 44);
        $filters = array();
        $functions = array("url" => 67);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'for', 'if'),
                array(),
                array('url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 36
        echo "

";
        // line 38
        list($context["icon_link"], $context["icon_image"], $context["link_to_front"]) =         array("", "", "");
        // line 39
        $context["fcount"] = 3;
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fields"]) ? $context["fields"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 42
            $context["fcount"] = ((isset($context["fcount"]) ? $context["fcount"] : null) - 1);
            // line 44
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-link")) {
                // line 45
                echo "
                ";
                // line 46
                $context["icon_link"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 51
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-symbol")) {
                // line 52
                echo "
                ";
                // line 53
                $context["icon_image"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 57
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-link-to-front-page")) {
                // line 58
                echo "
                ";
                // line 59
                $context["link_to_front"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 62
            echo "


    ";
            // line 65
            if (((isset($context["fcount"]) ? $context["fcount"] : null) == 0)) {
                // line 66
                echo "
           <a class=\"platformlogo_url\" href=\"";
                // line 67
                if (((isset($context["link_to_front"]) ? $context["link_to_front"] : null) == "True")) {
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getUrl("<front>")));
                    echo " ";
                } else {
                    echo " ";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["icon_link"]) ? $context["icon_link"] : null), "html", null, true));
                    echo " ";
                }
                echo "\" ><img class=\"logo_image_one\" src=\"";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["icon_image"]) ? $context["icon_image"] : null), "html", null, true));
                echo "\" alt=\"Home Page\" /></a>";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_381d5a4707bd87ca9e74655b4c4ab7f2cb269d8e10f7d6cc5886414a41d55436->leave($__internal_381d5a4707bd87ca9e74655b4c4ab7f2cb269d8e10f7d6cc5886414a41d55436_prof);

    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/views-view-fields--header_logo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 67,  91 => 66,  89 => 65,  84 => 62,  81 => 59,  78 => 58,  76 => 57,  73 => 53,  70 => 52,  68 => 51,  65 => 46,  62 => 45,  60 => 44,  58 => 42,  54 => 40,  52 => 39,  50 => 38,  46 => 36,);
    }

    public function getSource()
    {
        return "{#
/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
* details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This is view file created for the displaying footer logo in Footer.*
*                                                                             *
* Created by: Varun Madkaikar                                                 *
*                                                                             *
* Date: 20-JULY-2017                                                           *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version     Date        By                Description                       *
* --------------------------------------------------------------------------- *
* 1.0       20-07-17  Varun Madkaikar      Initial Version                    *
*                                                                             *
*                                                                             *
*******************************************************************************
**/
#}


{% set icon_link,icon_image,link_to_front = '','','' %}
{% set fcount = 3 %}
{% for field in fields -%}

\t{% set fcount = fcount -1 %}

        {%- if field.wrapper_attributes.class[1] == 'views-field-field-link' %}

                {%  set icon_link = field.content  %}

        {%- endif %}


        {%- if field.wrapper_attributes.class[1] == 'views-field-field-symbol' %}

                {%  set icon_image = field.content  %}

        {%- endif %}

        {%- if field.wrapper_attributes.class[1] == 'views-field-field-link-to-front-page' %}

                {%  set  link_to_front = field.content  %}

        {%- endif %}



    {% if(fcount == 0 ) %}

           <a class=\"platformlogo_url\" href=\"{% if(link_to_front == 'True') %}{{ url('<front>')}} {% else %} {{ icon_link }} {% endif %}\" ><img class=\"logo_image_one\" src=\"{{ icon_image }}\" alt=\"Home Page\" /></a>           
      
    {%- endif %}
    
{%- endfor %}
";
    }
}
