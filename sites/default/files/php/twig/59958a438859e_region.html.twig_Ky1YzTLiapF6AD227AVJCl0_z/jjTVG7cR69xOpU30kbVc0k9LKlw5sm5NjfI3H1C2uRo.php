<?php

/* themes/custom/iimbx/templates/region.html.twig */
class __TwigTemplate_bb22529e2dbe4ab4794caa7642296ecec27a236fd60dbb547d43303c4b74510e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4e02c0f206f31338ea28abfc062f7c52160f10db6b96ef6de018f9ea28e1ae7 = $this->env->getExtension("native_profiler");
        $__internal_b4e02c0f206f31338ea28abfc062f7c52160f10db6b96ef6de018f9ea28e1ae7->enter($__internal_b4e02c0f206f31338ea28abfc062f7c52160f10db6b96ef6de018f9ea28e1ae7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/region.html.twig"));

        $tags = array("if" => 39);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 38
        echo "
";
        // line 39
        if ((isset($context["content"]) ? $context["content"] : null)) {
            // line 40
            echo "  <div>
    ";
            // line 41
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["content"]) ? $context["content"] : null), "html", null, true));
            echo "
  </div>
";
        }
        
        $__internal_b4e02c0f206f31338ea28abfc062f7c52160f10db6b96ef6de018f9ea28e1ae7->leave($__internal_b4e02c0f206f31338ea28abfc062f7c52160f10db6b96ef6de018f9ea28e1ae7_prof);

    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/region.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 41,  51 => 40,  49 => 39,  46 => 38,);
    }

    public function getSource()
    {
        return "{#

/**
* This file is part of IIMBx-Drupal.
*
* IIMBx-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBx-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IITBombayX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file shows the different regions used in theme.               *
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
* Date: 20-JUl-2017                                                           *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date     By             Description                                 *
* --------------------------------------------------------------------------- *
* 1.0     20-07-17  Mangesh G      Initial Version                            *
*                                                                             *
*                                                                             *
*******************************************************************************
 */

#}

{% if content %}
  <div>
    {{ content }}
  </div>
{% endif %}
";
    }
}
