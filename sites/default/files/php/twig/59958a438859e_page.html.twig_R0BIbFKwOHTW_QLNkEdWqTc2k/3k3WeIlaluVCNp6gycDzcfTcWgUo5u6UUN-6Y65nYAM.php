<?php

/* themes/custom/iimbx/templates/page.html.twig */
class __TwigTemplate_0196ec15a5f1bd7a7d073536ae7274fed44dfb8c9f6dab62272fe97f3808b6c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_73b50c84c10540a9fe348706d6920249492447085853705c256fc9126eaf05bd = $this->env->getExtension("native_profiler");
        $__internal_73b50c84c10540a9fe348706d6920249492447085853705c256fc9126eaf05bd->enter($__internal_73b50c84c10540a9fe348706d6920249492447085853705c256fc9126eaf05bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/page.html.twig"));

        $tags = array("set" => 69, "if" => 70);
        $filters = array("raw" => 117);
        $functions = array("path" => 69, "file_url" => 111);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'if'),
                array('raw'),
                array('path', 'file_url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 69
        $context["current_url"] = $this->env->getExtension('drupal_core')->getPath("<current>");
        // line 70
        if ((isset($context["node"]) ? $context["node"] : null)) {
            echo " 
";
            // line 71
            $context["class_var"] = $this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_class_name", array()), "value", array());
        } else {
            // line 73
            echo "\t";
            if (twig_in_filter("course/about", (isset($context["current_url"]) ? $context["current_url"] : null))) {
                // line 74
                echo "\t\t";
                $context["class_var"] = "courses_about_page";
                // line 75
                echo "\t";
            } else {
                // line 76
                echo "\t\t";
                $context["class_var"] = "index-page";
                // line 77
                echo "\t";
            }
        }
        // line 79
        echo "
<div class=\"ltr ";
        // line 80
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["class_var"]) ? $context["class_var"] : null), "html", null, true));
        echo " lang_en\">
<div class=\"window-wrap\" dir=\"ltr\">
 <header id=\"global-navigation\" class=\"header-global\">
    <nav class=\"wrapper-header\" aria-label=\"Global\">

  
    <div id=\"auth_header\" style=\"display:none;\">
    \t<h1 class=\"logo\">
      \t\t";
        // line 88
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "rightlogo", array()), "html", null, true));
        echo "
    \t</h1>
      ";
        // line 90
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header_after_login", array()), "html", null, true));
        echo "
    </div>
 
    <div id=\"guest_header\" style=\"display:block;\">
        <h1 class=\"logo\">
            ";
        // line 95
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "headerlogo", array()), "html", null, true));
        echo "
        </h1>
         ";
        // line 97
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "
        <h1 class=\"platform_logo\">
         ";
        // line 99
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "rightlogo", array()), "html", null, true));
        echo "
        </h1>
    </div>
 
    </nav>
  </header>
      ";
        // line 105
        if ((isset($context["node"]) ? $context["node"] : null)) {
            // line 106
            echo "        ";
            if (((isset($context["class_var"]) ? $context["class_var"] : null) == "about")) {
                // line 107
                echo "          <div class=\"content-wrapper\" id=\"content\">
\t\t<div class=\"outer_container\">\t\t\t
\t\t  <div class=\"main_content\">
                     <div class=\"banner\">
                        <img class=\"main_banner\" src=\"";
                // line 111
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_banner", array()), "entity", array()), "fileuri", array()))), "html", null, true));
                echo "\">
                     </div>
                         <div class=\"tagline\">
                        <h2>";
                // line 114
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_about_tagline", array()), "value", array()), "html", null, true));
                echo "</h2>
                     </div>
                  </div>
                  ";
                // line 117
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array())));
                echo "
                </div>
          </div>

\t";
            } elseif ((            // line 121
(isset($context["class_var"]) ? $context["class_var"] : null) == "contact_us")) {
                // line 122
                echo "                <div class=\"content-wrapper\" id=\"content\">
                  <section class=\"container about\">
\t\t\t<div class=\"main_content\">
\t\t\t<div class=\"banner\"><img src=\"";
                // line 125
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_banner", array()), "entity", array()), "fileuri", array()))), "html", null, true));
                echo "\" width=\"1200px\"></div>
\t\t\t<div class=\"wrapper\">
          \t\t<div class=\"wrapper-container\">
\t   \t\t<div class=\"wrapper-inner\">
\t\t\t<div class=\"inner-class\">
\t\t\t<div class=\"inner-content\">
\t\t\t <section class=\"inner-section\">
\t\t\t  <h2 class=\"field-page-tagline view-mode-full\" >";
                // line 132
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_headline", array()), "value", array()), "html", null, true));
                echo "</h2>
\t\t\t  <div class=\"field-page-body view-mode-full\">
                                      ";
                // line 134
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "body", array()), "value", array())));
                echo "
\t\t\t  </div>
\t\t\t </section>
\t\t\t</div>
\t\t\t</div>
\t\t        </div>
\t                </div>
                 \t</div>
                        </div>
                  </section>
                </div>
         ";
            } elseif (((((((((((((            // line 145
(isset($context["class_var"]) ? $context["class_var"] : null) == "corporate_faq") || ((isset($context["class_var"]) ? $context["class_var"] : null) == "corporate_details")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "corporate_overview")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "faculty_faq")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "faculty_overview")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "faculty_details")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "academic_faq")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "academic_overview")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "academic_details")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "programme_faq")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "programme_overview")) || ((isset($context["class_var"]) ? $context["class_var"] : null) == "programme_details"))) {
                // line 146
                echo "
  
       <div class=\"content-wrapper\" id=\"content\">
          <div class=\"outer_container\">
\t      <div class=\"main_content\">
\t\t<div class=\"banner\">
\t           <img class=\"main_banner\" src=\"";
                // line 152
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_banner", array()), "entity", array()), "fileuri", array()))), "html", null, true));
                echo "\">
\t\t</div>
\t\t<div class=\"tagline\">
\t\t\t<h2>";
                // line 155
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_headline", array()), "value", array()), "html", null, true));
                echo "</h2>
\t\t</div>
\t\t<div class=\"content\">
                  <div class=\"left_content\">
\t\t    <div class=\"sidebar\">
                     ";
                // line 160
                if (($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_sidebar_page", array()), "value", array()) == "corporate")) {
                    // line 161
                    echo "                               ";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "corporate_sidebar", array()), "html", null, true));
                    echo "
                     ";
                } elseif (($this->getAttribute($this->getAttribute(                // line 162
(isset($context["node"]) ? $context["node"] : null), "field_sidebar_page", array()), "value", array()) == "academic")) {
                    // line 163
                    echo "                               ";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "academic_sidebar", array()), "html", null, true));
                    echo "
                     ";
                } else {
                    // line 165
                    echo "                               ";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "faculty_sidebar", array()), "html", null, true));
                    echo "
                     ";
                }
                // line 167
                echo "\t            </div>
\t\t  </div>

\t\t\t<div class=\"right_content\">
\t\t\t\t<div class=\"right_content_text\">

                                     ";
                // line 173
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "body", array()), "value", array())));
                echo "\t\t\t
\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
             </div>
         </div>
      </div>

     ";
            } elseif (((            // line 182
(isset($context["class_var"]) ? $context["class_var"] : null) == "tos_page") || ((isset($context["class_var"]) ? $context["class_var"] : null) == "privacy_page"))) {
                // line 183
                echo "     <div class=\"content-wrapper\" id=\"content\">
       <div class=\"wrapper\">
         <div class=\"container\">
           <div class=\"node-content\">
             <div class=\"main-content\">
                <h2 class=\"fieldview\">";
                // line 188
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_headline", array()), "value", array())));
                echo "</h2>
                   <div class=\"fieldbody\">
                       ";
                // line 190
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "body", array()), "value", array())));
                echo "
                    </div>
             </div>
           </div>
        </div>
      </div>
    </div>

   ";
            } elseif ((            // line 198
(isset($context["class_var"]) ? $context["class_var"] : null) == "learners_faq")) {
                // line 199
                echo "   <div class=\"content-wrapper\" id=\"content\">
      <section class=\"container about\">
\t<div class=\"main_content\">
           <div class=\"banner\"><img src=\"";
                // line 202
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_banner", array()), "entity", array()), "fileuri", array()))), "html", null, true));
                echo "\" width=\"1200px\"></div>
\t   <div class=\"wrapper\">
\t   <div class=\"wrapper-container\">
\t      <div class=\"wrapper-inner\">
\t         <div class=\"inner-class\">
\t\t    <div class=\"inner-content\">\t\t\t\t\t\t\t\t<section class=\"inner-section\">                                  \t\t\t<h2 class=\"field-page-tagline view-mode-full\">Learners' FAQ</h2>
 \t\t\t\t<div class=\"field-page-body view-mode-full\">
\t\t\t\t ";
                // line 209
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "body", array()), "value", array())));
                echo "                                                        <div>
\t\t\t\t\t<section class=\"ac-container\">
\t\t\t\t\t\t";
                // line 211
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "main_faq", array()), "html", null, true));
                echo "
\t\t\t\t\t</section>
                                  </div>
                                </div>
\t\t\t</section>
\t\t    </div>
\t\t</div>
             </div>
         </div>
       </div>
     </div>
    </section>
  </div>


   ";
            } elseif ((            // line 226
(isset($context["class_var"]) ? $context["class_var"] : null) == "programmes")) {
                // line 227
                echo "     <div class=\"content-wrapper\" id=\"content\">
       <div class=\"outer_container\">
\t<div class=\"main_content\">
          <div class=\"banner\">
\t    <img class=\"main_banner\" src=\"";
                // line 231
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_banner", array()), "entity", array()), "fileuri", array()))), "html", null, true));
                echo "\">
\t  </div>
          <div class=\"tagline\">
\t\t<h2>IIMBx Programmes</h2>
\t  </div>
          <div class=\"content\">
              ";
                // line 237
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "body", array()), "value", array())));
                echo "
          </div>
\t</div>
\t<div class=\"container_one\">
          <div class=\"container_two\">
 \t   <ul>
              ";
                // line 243
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "programmes_icons", array()), "html", null, true));
                echo "     
           </ul>
         </div>
\t</div>
       <div class=\"main_content\">
\t<div class=\"tagline_inner\">
          <h2>IIMBx Course Highlights</h2>
\t</div>
\t<div class=\"image_layer_two\">
          <ul class=\"course_highlights\">
             ";
                // line 253
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "course_highlights", array()), "html", null, true));
                echo "
\t  </ul>
\t</div>
        <div class=\"tagline_inner\">
\t  <h2>Course Offerings</h2>
\t</div>
\t<div class=\"image_layer_one\">
        \t<ul class=\"course_list\">
\t\t\t";
                // line 261
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "course_offerings", array()), "html", null, true));
                echo "
                </ul>
\t</div>
        <div class=\"courses-more\">
\t <a class=\"courses-more-cta\" href=\"/courses\"> View all Courses >> </a>
\t</div>
     </div>
  </div> 
</div>
  ";
            } elseif ((            // line 270
(isset($context["class_var"]) ? $context["class_var"] : null) == "index-page")) {
                // line 271
                echo "           <div class=\"content-wrapper\" id=\"content\">
\t\t<main id=\"main\" aria-label=\"Content\" tabindex=\"-1\">
\t\t    <section class=\"home\">
\t\t      <header style=\"background-image: url(";
                // line 274
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), array($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["node"]) ? $context["node"] : null), "field_banner", array()), "entity", array()), "fileuri", array()))), "html", null, true));
                echo ") !important;\">
\t\t      </header>

                   ";
                // line 284
                echo "           <section class=\"courses-container\">
              <section class=\"highlighted-courses\">
\t\t 
                      ";
                // line 287
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "index_course_list", array()), "html", null, true));
                echo "

              </section>
           </section>
   </section>
</main>

<div class=\"outer_container\">
    <div class=\"main_content\">
             ";
                // line 296
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "home_icons", array()), "html", null, true));
                echo "
    </div>
</div>


";
            }
            // line 302
            echo "
";
        } elseif (twig_in_filter("courses",         // line 303
(isset($context["current_url"]) ? $context["current_url"] : null))) {
            // line 304
            echo "<div class=\"inner_courses\">
<div class=\"content-wrapper\" id=\"content\">
     <main id=\"main\" aria-label=\"Content\" tabindex=\"-1\">
        <section class=\"home\">
           <section class=\"courses-container\">
              <section class=\"highlighted-courses\">
                  <section class=\"courses\">

                                         <div class=\"row is-flex\">
                                              <div class=\"col-sm-3 table_column01\" >
                                                <div class=\"coursesidebar\" id=\"navbarCollapsec\" >
                                                  <div class=\"filtertitle\"> Refine Your Search </div>
                                                    ";
            // line 316
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "search_facets", array()), "html", null, true));
            echo "
                                                  </div>
                                                </div>

                                                <div class=\"col-sm-9 table_column02\">
                                                  <section class=\"inner-section\">
                                                     <ul class=\"courses-listing\">
\t\t\t\t\t                      ";
            // line 323
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
            echo "
                         \t\t\t\t</ul>

                                                  </section>
                                                </div>

                                              </div>



                  </section>
              </section>
           </section>
        </section>
     </main>
</div>
</div>
";
        } elseif (twig_in_filter("dcourse",         // line 340
(isset($context["current_url"]) ? $context["current_url"] : null))) {
            // line 341
            echo "<div class=\"inner_courses\">
<div class=\"content-wrapper\" id=\"content\">
     <main id=\"main\" aria-label=\"Content\" tabindex=\"-1\">
        <section class=\"home\">
           <section class=\"courses-container\">
              <section class=\"highlighted-courses\">
                  <section class=\"courses\">
                     <div class=\"row\">

                      <div class=\"coursesidebar\" id=\"navbarCollapsec\" >
                          ";
            // line 351
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "search_facets", array()), "html", null, true));
            echo "
                      </div>

                      <section class=\"inner-section\">
                         <ul class=\"courses-listing\">
                            ";
            // line 356
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
            echo "
                         </ul>
                      </section>
                    </div>
                  </section>
              </section>
           </section>
        </section>
     </main>
</div>
</div>




";
        } else {
            // line 372
            echo "<div class=\"content-wrapper\" id=\"content\">
";
            // line 375
            echo "        

";
            // line 377
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
            echo "
    ";
            // line 380
            echo "</div>

";
        }
        // line 383
        echo "\t<div class=\"wrapper wrapper-footer\">
\t  <footer>
\t    <div class=\"colophon\">
\t      <nav class=\"nav-colophon\"> ";
        // line 386
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
        echo " </nav>
\t      <div class=\"colophon-about\">
\t\t<div><div style=\"float:left;\" class=\"footer_class\" >";
        // line 388
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footerlogo", array()), "html", null, true));
        echo " </div> <div style=\"float:left;width:570px;\">";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footertext", array()), "html", null, true));
        echo " </div></div>
\t      </div>
\t    </div>

\t    <div class=\"references\">
\t      ";
        // line 393
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sociallinks", array()), "html", null, true));
        echo "
\t      ";
        // line 394
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "legallinks", array()), "html", null, true));
        echo "
\t    </div>
\t    <div class=\"open-edx_logo\">\t\t
\t      ";
        // line 397
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "edxlink", array()), "html", null, true));
        echo "
\t    </div>
\t  </footer>
\t</div>
    </div>
  </div>

";
        
        $__internal_73b50c84c10540a9fe348706d6920249492447085853705c256fc9126eaf05bd->leave($__internal_73b50c84c10540a9fe348706d6920249492447085853705c256fc9126eaf05bd_prof);

    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  550 => 397,  544 => 394,  540 => 393,  530 => 388,  525 => 386,  520 => 383,  515 => 380,  511 => 377,  507 => 375,  504 => 372,  485 => 356,  477 => 351,  465 => 341,  463 => 340,  443 => 323,  433 => 316,  419 => 304,  417 => 303,  414 => 302,  405 => 296,  393 => 287,  388 => 284,  382 => 274,  377 => 271,  375 => 270,  363 => 261,  352 => 253,  339 => 243,  330 => 237,  321 => 231,  315 => 227,  313 => 226,  295 => 211,  290 => 209,  280 => 202,  275 => 199,  273 => 198,  262 => 190,  257 => 188,  250 => 183,  248 => 182,  236 => 173,  228 => 167,  222 => 165,  216 => 163,  214 => 162,  209 => 161,  207 => 160,  199 => 155,  193 => 152,  185 => 146,  183 => 145,  169 => 134,  164 => 132,  154 => 125,  149 => 122,  147 => 121,  140 => 117,  134 => 114,  128 => 111,  122 => 107,  119 => 106,  117 => 105,  108 => 99,  103 => 97,  98 => 95,  90 => 90,  85 => 88,  74 => 80,  71 => 79,  67 => 77,  64 => 76,  61 => 75,  58 => 74,  55 => 73,  52 => 71,  48 => 70,  46 => 69,);
    }

    public function getSource()
    {
        return "{#

/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file is created for the display main menus appear in Header.  *
*                                                                             *
* Created by: Varun Madkaikar                                                 *
*                                                                             *
* Date: 19-07-2017                                                            *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date     By                Description                              *
* --------------------------------------------------------------------------- *
* 1.0     19-07-17  Varun Madkaikar   Initial Version                         *
*                                                                             *
*                                                                             *
*******************************************************************************
*/

/**
 * @file
 * Theme override to display a single page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.html.twig template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - base_path: The base URL path of the Drupal installation. Will usually be
 *   \"/\" unless you have installed Drupal in a sub-directory.
 * - is_front: A flag indicating if the current page is the front page.
 * - logged_in: A flag indicating if the user is registered and signed in.
 * - is_admin: A flag indicating if the user has permission to access
 *   administration pages.
 *
 * Site identity:
 * - front_page: The URL of the front page. Use this instead of base_path when
 *   linking to the front page. This includes the language domain or prefix.
 *
 * Page content (in order of occurrence in the default page.html.twig):
 * - node: Fully loaded node, if there is an automatically-loaded node
 *   associated with the page and the node ID is the second argument in the
 *   page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 *
 * @see template_preprocess_page()
 * @see html.html.twig
 */
#}
{% set current_url = path('<current>') %}
{% if(node) %} 
{% set class_var = node.field_class_name.value  %}
{% else %}
\t{% if( \"course/about\" in current_url ) %}
\t\t{% set class_var = 'courses_about_page' %}
\t{% else %}
\t\t{% set class_var = 'index-page' %}
\t{% endif %}
{% endif %}

<div class=\"ltr {{ class_var }} lang_en\">
<div class=\"window-wrap\" dir=\"ltr\">
 <header id=\"global-navigation\" class=\"header-global\">
    <nav class=\"wrapper-header\" aria-label=\"Global\">

  
    <div id=\"auth_header\" style=\"display:none;\">
    \t<h1 class=\"logo\">
      \t\t{{ page.rightlogo }}
    \t</h1>
      {{ page.header_after_login }}
    </div>
 
    <div id=\"guest_header\" style=\"display:block;\">
        <h1 class=\"logo\">
            {{ page.headerlogo }}
        </h1>
         {{ page.header }}
        <h1 class=\"platform_logo\">
         {{ page.rightlogo }}
        </h1>
    </div>
 
    </nav>
  </header>
      {% if(node) %}
        {% if (class_var =='about')  %}
          <div class=\"content-wrapper\" id=\"content\">
\t\t<div class=\"outer_container\">\t\t\t
\t\t  <div class=\"main_content\">
                     <div class=\"banner\">
                        <img class=\"main_banner\" src=\"{{ file_url(node.field_banner.entity.fileuri) }}\">
                     </div>
                         <div class=\"tagline\">
                        <h2>{{ node.field_about_tagline.value }}</h2>
                     </div>
                  </div>
                  {{ page.content|raw }}
                </div>
          </div>

\t{% elseif (class_var =='contact_us')  %}
                <div class=\"content-wrapper\" id=\"content\">
                  <section class=\"container about\">
\t\t\t<div class=\"main_content\">
\t\t\t<div class=\"banner\"><img src=\"{{ file_url(node.field_banner.entity.fileuri)}}\" width=\"1200px\"></div>
\t\t\t<div class=\"wrapper\">
          \t\t<div class=\"wrapper-container\">
\t   \t\t<div class=\"wrapper-inner\">
\t\t\t<div class=\"inner-class\">
\t\t\t<div class=\"inner-content\">
\t\t\t <section class=\"inner-section\">
\t\t\t  <h2 class=\"field-page-tagline view-mode-full\" >{{ node.field_headline.value }}</h2>
\t\t\t  <div class=\"field-page-body view-mode-full\">
                                      {{ node.body.value|raw }}
\t\t\t  </div>
\t\t\t </section>
\t\t\t</div>
\t\t\t</div>
\t\t        </div>
\t                </div>
                 \t</div>
                        </div>
                  </section>
                </div>
         {% elseif (class_var =='corporate_faq' or class_var =='corporate_details' or class_var =='corporate_overview' or class_var =='faculty_faq' or class_var =='faculty_overview' or class_var =='faculty_details' or class_var =='academic_faq' or class_var =='academic_overview' or class_var == 'academic_details' or class_var =='programme_faq' or class_var =='programme_overview' or class_var == 'programme_details')  %}

  
       <div class=\"content-wrapper\" id=\"content\">
          <div class=\"outer_container\">
\t      <div class=\"main_content\">
\t\t<div class=\"banner\">
\t           <img class=\"main_banner\" src=\"{{ file_url(node.field_banner.entity.fileuri)}}\">
\t\t</div>
\t\t<div class=\"tagline\">
\t\t\t<h2>{{ node.field_headline.value }}</h2>
\t\t</div>
\t\t<div class=\"content\">
                  <div class=\"left_content\">
\t\t    <div class=\"sidebar\">
                     {% if (node.field_sidebar_page.value == 'corporate') %}
                               {{ page.corporate_sidebar }}
                     {% elseif (node.field_sidebar_page.value == 'academic') %}
                               {{ page.academic_sidebar }}
                     {% else %}
                               {{ page.faculty_sidebar }}
                     {% endif %}
\t            </div>
\t\t  </div>

\t\t\t<div class=\"right_content\">
\t\t\t\t<div class=\"right_content_text\">

                                     {{ node.body.value|raw }}\t\t\t
\t
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
             </div>
         </div>
      </div>

     {% elseif (class_var =='tos_page' or class_var == 'privacy_page')  %}
     <div class=\"content-wrapper\" id=\"content\">
       <div class=\"wrapper\">
         <div class=\"container\">
           <div class=\"node-content\">
             <div class=\"main-content\">
                <h2 class=\"fieldview\">{{ node.field_headline.value|raw }}</h2>
                   <div class=\"fieldbody\">
                       {{ node.body.value|raw }}
                    </div>
             </div>
           </div>
        </div>
      </div>
    </div>

   {% elseif (class_var =='learners_faq')  %}
   <div class=\"content-wrapper\" id=\"content\">
      <section class=\"container about\">
\t<div class=\"main_content\">
           <div class=\"banner\"><img src=\"{{ file_url(node.field_banner.entity.fileuri) }}\" width=\"1200px\"></div>
\t   <div class=\"wrapper\">
\t   <div class=\"wrapper-container\">
\t      <div class=\"wrapper-inner\">
\t         <div class=\"inner-class\">
\t\t    <div class=\"inner-content\">\t\t\t\t\t\t\t\t<section class=\"inner-section\">                                  \t\t\t<h2 class=\"field-page-tagline view-mode-full\">Learners' FAQ</h2>
 \t\t\t\t<div class=\"field-page-body view-mode-full\">
\t\t\t\t {{ node.body.value|raw }}                                                        <div>
\t\t\t\t\t<section class=\"ac-container\">
\t\t\t\t\t\t{{ page.main_faq }}
\t\t\t\t\t</section>
                                  </div>
                                </div>
\t\t\t</section>
\t\t    </div>
\t\t</div>
             </div>
         </div>
       </div>
     </div>
    </section>
  </div>


   {% elseif (class_var =='programmes')  %}
     <div class=\"content-wrapper\" id=\"content\">
       <div class=\"outer_container\">
\t<div class=\"main_content\">
          <div class=\"banner\">
\t    <img class=\"main_banner\" src=\"{{ file_url(node.field_banner.entity.fileuri)}}\">
\t  </div>
          <div class=\"tagline\">
\t\t<h2>IIMBx Programmes</h2>
\t  </div>
          <div class=\"content\">
              {{ node.body.value|raw }}
          </div>
\t</div>
\t<div class=\"container_one\">
          <div class=\"container_two\">
 \t   <ul>
              {{ page.programmes_icons }}     
           </ul>
         </div>
\t</div>
       <div class=\"main_content\">
\t<div class=\"tagline_inner\">
          <h2>IIMBx Course Highlights</h2>
\t</div>
\t<div class=\"image_layer_two\">
          <ul class=\"course_highlights\">
             {{ page.course_highlights }}
\t  </ul>
\t</div>
        <div class=\"tagline_inner\">
\t  <h2>Course Offerings</h2>
\t</div>
\t<div class=\"image_layer_one\">
        \t<ul class=\"course_list\">
\t\t\t{{ page.course_offerings }}
                </ul>
\t</div>
        <div class=\"courses-more\">
\t <a class=\"courses-more-cta\" href=\"/courses\"> View all Courses >> </a>
\t</div>
     </div>
  </div> 
</div>
  {% elseif(class_var == 'index-page') %}
           <div class=\"content-wrapper\" id=\"content\">
\t\t<main id=\"main\" aria-label=\"Content\" tabindex=\"-1\">
\t\t    <section class=\"home\">
\t\t      <header style=\"background-image: url({{ file_url(node.field_banner.entity.fileuri)}}) !important;\">
\t\t      </header>

                   {#<div class=\"tagline_container\">
\t\t      <div class=\"tagline_content\">
\t\t\t<div class=\"tagline\">
                          <h2 class=\"tagline_text\" id=\"tagline\">Course Catalog</h2>
\t\t\t</div>
\t\t      </div>
\t          </div>#}
           <section class=\"courses-container\">
              <section class=\"highlighted-courses\">
\t\t 
                      {{ page.index_course_list }}

              </section>
           </section>
   </section>
</main>

<div class=\"outer_container\">
    <div class=\"main_content\">
             {{ page.home_icons }}
    </div>
</div>


{% endif %}

{% elseif( 'courses' in current_url ) %}
<div class=\"inner_courses\">
<div class=\"content-wrapper\" id=\"content\">
     <main id=\"main\" aria-label=\"Content\" tabindex=\"-1\">
        <section class=\"home\">
           <section class=\"courses-container\">
              <section class=\"highlighted-courses\">
                  <section class=\"courses\">

                                         <div class=\"row is-flex\">
                                              <div class=\"col-sm-3 table_column01\" >
                                                <div class=\"coursesidebar\" id=\"navbarCollapsec\" >
                                                  <div class=\"filtertitle\"> Refine Your Search </div>
                                                    {{page.search_facets}}
                                                  </div>
                                                </div>

                                                <div class=\"col-sm-9 table_column02\">
                                                  <section class=\"inner-section\">
                                                     <ul class=\"courses-listing\">
\t\t\t\t\t                      {{ page.content}}
                         \t\t\t\t</ul>

                                                  </section>
                                                </div>

                                              </div>



                  </section>
              </section>
           </section>
        </section>
     </main>
</div>
</div>
{% elseif( 'dcourse' in current_url ) %}
<div class=\"inner_courses\">
<div class=\"content-wrapper\" id=\"content\">
     <main id=\"main\" aria-label=\"Content\" tabindex=\"-1\">
        <section class=\"home\">
           <section class=\"courses-container\">
              <section class=\"highlighted-courses\">
                  <section class=\"courses\">
                     <div class=\"row\">

                      <div class=\"coursesidebar\" id=\"navbarCollapsec\" >
                          {{ page.search_facets }}
                      </div>

                      <section class=\"inner-section\">
                         <ul class=\"courses-listing\">
                            {{ page.content}}
                         </ul>
                      </section>
                    </div>
                  </section>
              </section>
           </section>
        </section>
     </main>
</div>
</div>




{% else %}
<div class=\"content-wrapper\" id=\"content\">
{#      <section class=\"container about\">
        <div class=\"main_content\">
  #}        

{{ page.content}}
    {#    </div>
     </section> #}
</div>

{% endif %}
\t<div class=\"wrapper wrapper-footer\">
\t  <footer>
\t    <div class=\"colophon\">
\t      <nav class=\"nav-colophon\"> {{ page.footer}} </nav>
\t      <div class=\"colophon-about\">
\t\t<div><div style=\"float:left;\" class=\"footer_class\" >{{ page.footerlogo }} </div> <div style=\"float:left;width:570px;\">{{ page.footertext }} </div></div>
\t      </div>
\t    </div>

\t    <div class=\"references\">
\t      {{ page.sociallinks }}
\t      {{ page.legallinks }}
\t    </div>
\t    <div class=\"open-edx_logo\">\t\t
\t      {{ page.edxlink }}
\t    </div>
\t  </footer>
\t</div>
    </div>
  </div>

";
    }
}
