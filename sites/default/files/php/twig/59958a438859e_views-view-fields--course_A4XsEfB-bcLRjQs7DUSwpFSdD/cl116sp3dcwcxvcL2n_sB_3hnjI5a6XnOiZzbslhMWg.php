<?php

/* themes/custom/iimbx/templates/views-view-fields--course_about--page.html.twig */
class __TwigTemplate_db68b66c033492c5232c16a46ca37b1e0753752dd9efc091b02103c5b89797bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb0c88a0f942edf4030ab56ce35b3b0be0ede21f44151d351ec8a627fe87e5a2 = $this->env->getExtension("native_profiler");
        $__internal_bb0c88a0f942edf4030ab56ce35b3b0be0ede21f44151d351ec8a627fe87e5a2->enter($__internal_bb0c88a0f942edf4030ab56ce35b3b0be0ede21f44151d351ec8a627fe87e5a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/views-view-fields--course_about--page.html.twig"));

        $tags = array("set" => 54, "for" => 55, "if" => 58);
        $filters = array("replace" => 225);
        $functions = array("url" => 161);

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'for', 'if'),
                array('replace'),
                array('url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 53
        echo "
";
        // line 54
        $context["fcount"] = 11;
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["fields"]) ? $context["fields"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 57
            $context["fcount"] = ((isset($context["fcount"]) ? $context["fcount"] : null) - 1);
            // line 58
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-link")) {
                // line 59
                echo "  ";
                $context["course_link"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 62
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-image")) {
                // line 63
                echo "  ";
                $context["image_link"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 66
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-code")) {
                echo "  ";
                $context["course_code"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 69
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-name")) {
                // line 70
                echo "  ";
                $context["course_name"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 73
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-display-string")) {
                // line 74
                echo "  ";
                $context["start_date"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 77
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-end")) {
                // line 78
                echo "  ";
                $context["end_date"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 81
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-organisation")) {
                // line 82
                echo "  ";
                $context["organisation"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 85
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-effort")) {
                // line 86
                echo "  ";
                $context["effort"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 89
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-course-video")) {
                // line 90
                echo "  ";
                $context["video"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 93
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-enrollment-end")) {
                // line 94
                echo "  ";
                $context["enrollment_end"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 97
            if (($this->getAttribute($this->getAttribute($this->getAttribute($context["field"], "wrapper_attributes", array()), "class", array()), 1, array(), "array") == "views-field-field-enrollment-start")) {
                // line 98
                echo "  ";
                $context["enrollment_start"] = $this->getAttribute($context["field"], "content", array());
            }
            // line 100
            echo "


  ";
            // line 103
            if (((isset($context["fcount"]) ? $context["fcount"] : null) == 0)) {
                // line 104
                echo "     <input type='hidden' value=\"";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_link"]) ? $context["course_link"] : null), "html", null, true));
                echo "\" id=\"course_id\"/>
     <input type='hidden' value=\"";
                // line 105
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["edx_site_path"]) ? $context["edx_site_path"] : null), "html", null, true));
                echo "\" id=\"edx_site_path\"/>
     <input type='hidden' value=\"";
                // line 106
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["enrollment_start"]) ? $context["enrollment_start"] : null), "html", null, true));
                echo "\" id=\"enrollment_start\"/>
     <input type='hidden' value=\"";
                // line 107
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["enrollment_end"]) ? $context["enrollment_end"] : null), "html", null, true));
                echo "\" id=\"enrollment_end\"/>
     <input type='hidden' value=\"";
                // line 108
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_name"]) ? $context["course_name"] : null), "html", null, true));
                echo "\" id=\"course_name\"/>

      <section class=\"course-info\">
  \t<header class=\"course-profile\">
\t    <div class=\"intro-inner-wrapper\">
\t      <div class=\"table\">
\t      <section class=\"intro\">
        \t<div class=\"heading-group\">
\t          <h1> ";
                // line 116
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["site_path"]) ? $context["site_path"] : null), "html", null, true));
                echo "
        \t    ";
                // line 117
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_name"]) ? $context["course_name"] : null), "html", null, true));
                echo " 
\t            <a href=\"#\">";
                // line 118
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["organisation"]) ? $context["organisation"] : null), "html", null, true));
                echo "</a>
        \t  </h1>
\t        </div>

        \t<div class=\"main-cta\">
        <a href=\"#\" class=\"register\">Enroll in ";
                // line 123
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_code"]) ? $context["course_code"] : null), "html", null, true));
                echo " </a><div id=\"register_error\"/> 
\t        </div>

      </section>


   ";
                // line 129
                if (((isset($context["video"]) ? $context["video"] : null) != "")) {
                    // line 130
                    echo "      <a class=\"media\" href=\"#video-modal\" rel=\"leanModal\">
\t<div class=\"hero\">
\t<img src=\" ";
                    // line 132
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["edx_site_path"]) ? $context["edx_site_path"] : null), "html", null, true));
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["image_link"]) ? $context["image_link"] : null), "html", null, true));
                    echo "\" alt=\"";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_code"]) ? $context["course_code"] : null), "html", null, true));
                    echo "\" />
        <div class=\"play-intro\"></div>
\t</div>
     </a>
   ";
                } else {
                    // line 137
                    echo "      <div class=\"media\">
\t<div class=\"hero\">
\t<img src=\" ";
                    // line 139
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["edx_site_path"]) ? $context["edx_site_path"] : null), "html", null, true));
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["image_link"]) ? $context["image_link"] : null), "html", null, true));
                    echo "\" alt=\"";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_code"]) ? $context["course_code"] : null), "html", null, true));
                    echo "\" />
        </div>
     </div>
   ";
                }
                // line 143
                echo "
    </div>
      </div>
  </header>

  <div class=\"container\">
    <div class=\"details\">

      <div class=\"inner-wrapper\" id=\"about_data\">
      </div>
    </div>

    <div class=\"course-sidebar\">
      <div class=\"course-summary\">
   <header>
    <div class=\"social-sharing\">
     <div class=\"sharing-message\">Share with friends and family!</div>
      
      <a href=\"http://twitter.com/intent/tweet?text=I+just+enrolled+in+";
                // line 161
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_code"]) ? $context["course_code"] : null), "html", null, true));
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_name"]) ? $context["course_name"] : null), "html", null, true));
                echo "+through+https://twitter.com/iimbxonline:+";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getUrl("<front>")));
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_link"]) ? $context["course_link"] : null), "html", null, true));
                echo "/about\" class=\"share\">


       <span class=\"icon fa fa-twitter\" aria-hidden=\"true\"></span><span class=\"sr\">Tweet that you've enrolled in this course</span>
      </a>

      <a href=\"https://www.facebook.com/iimbxonline/\" class=\"share\">
        <span class=\"icon fa fa-thumbs-up\" aria-hidden=\"true\"></span><span class=\"sr\">Post a Facebook message to say you've enrolled in this course</span>
      </a>
      <a href=\"mailto:?subject=Take%20a%20course%20with%20IIMBX%20online&body=I%20just%20enrolled%20in%20";
                // line 170
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_code"]) ? $context["course_code"] : null), "html", null, true));
                echo "%20";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_name"]) ? $context["course_name"] : null), "html", null, true));
                echo "%20through%20IIMBx%20";
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar($this->env->getExtension('drupal_core')->getUrl("<front>")));
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_link"]) ? $context["course_link"] : null), "html", null, true));
                echo "/about\" class=\"share\">
        <span class=\"icon fa fa-envelope\" aria-hidden=\"true\"></span><span class=\"sr\">Email someone to say you've enrolled in this course</span>
      </a>
  </div>
</header>


        <ol class=\"important-dates\">
          ";
                // line 178
                if (((isset($context["course_code"]) ? $context["course_code"] : null) != "")) {
                    echo "         
<li class=\"important-dates-item\"><span class=\"icon fa fa-info-circle\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Course Number</p><span class=\"important-dates-item-text course-number\">";
                    // line 179
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_code"]) ? $context["course_code"] : null), "html", null, true));
                    echo "</span></li>
          ";
                }
                // line 181
                echo "          ";
                if (((isset($context["start_date"]) ? $context["start_date"] : null) != "")) {
                    // line 182
                    echo "            <li class=\"important-dates-item\"><span class=\"icon fa fa-calendar\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Classes Start</p><span class=\"important-dates-item-text start-date\">";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["start_date"]) ? $context["start_date"] : null), "html", null, true));
                    echo "</span></li>
          ";
                }
                // line 184
                echo "          ";
                if (((isset($context["end_date"]) ? $context["end_date"] : null) != "")) {
                    // line 185
                    echo "            <li class=\"important-dates-item\"><span class=\"icon fa fa-calendar\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Classes End</p><span class=\"important-dates-item-text start-date\">";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["end_date"]) ? $context["end_date"] : null), "html", null, true));
                    echo "</span></li>
          ";
                }
                // line 187
                echo "           ";
                if (((isset($context["effort"]) ? $context["effort"] : null) != "")) {
                    // line 188
                    echo "            <li class=\"important-dates-item\"><span class=\"icon fa fa-pencil\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Estimated Effort</p><span class=\"important-dates-item-text effort\">";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["effort"]) ? $context["effort"] : null), "html", null, true));
                    echo "</span></li>
          ";
                }
                // line 189
                echo "    


        </ol>
    </div>

      <div class=\"coursetalk-read-reviews\">
          <div id=\"ct-custom-read-review-widget\" data-provider=\"iimbx\" data-course=\"IIMBx_";
                // line 196
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_code"]) ? $context["course_code"] : null), "html", null, true));
                echo "\">

<iframe id=\"ct_widget_iframe\" class=\"ct-next-gen-widget\" src=\"https://www.coursetalk.com/widgets/v2.0/read/iframe-review-widget/nextgen/iimbx/IIMBx_";
                // line 198
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_code"]) ? $context["course_code"] : null), "html", null, true));
                echo "\" scrolling=\"no\">
</iframe>
</div>
      </div>


  </div>

  </div>
</div>

  <div style=\"display:none;\">
    <form id=\"class_enroll_form\" method=\"post\" data-remote=\"true\" action=\"/change_enrollment\">
       <fieldset class=\"enroll_fieldset\">
         <legend class=\"sr\">Enroll</legend>
         <input name=\"course_id\" type=\"hidden\" value=\"";
                // line 213
                echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["course_link"]) ? $context["course_link"] : null), "html", null, true));
                echo "\">
         <input name=\"enrollment_action\" type=\"hidden\" value=\"enroll\">
       </fieldset>
       <div class=\"submit\">
        <input name=\"submit\" type=\"submit\" value=\"enroll\">
       </div>
    </form>
  </div>

<div id=\"video-modal\" class=\"modal video-modal\">
<div class=\"inner-wrapper\">
";
                // line 224
                if (((isset($context["video"]) ? $context["video"] : null) != "")) {
                    // line 225
                    echo "<iframe title=\"YouTube Video\" src=\"";
                    echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, twig_replace_filter((isset($context["video"]) ? $context["video"] : null), array("watch?v=" => "embed/")), "html", null, true));
                    echo "\" allowfullscreen=\"\" width=\"560\" height=\"315\" frameborder=\"0\">
</iframe>
";
                }
                // line 231
                echo "</div>
</div>

<div id=\"lean_overlay\"></div>    
</div>
</section>
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 239
        echo "


";
        
        $__internal_bb0c88a0f942edf4030ab56ce35b3b0be0ede21f44151d351ec8a627fe87e5a2->leave($__internal_bb0c88a0f942edf4030ab56ce35b3b0be0ede21f44151d351ec8a627fe87e5a2_prof);

    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/views-view-fields--course_about--page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  369 => 239,  356 => 231,  349 => 225,  347 => 224,  333 => 213,  315 => 198,  310 => 196,  301 => 189,  295 => 188,  292 => 187,  286 => 185,  283 => 184,  277 => 182,  274 => 181,  269 => 179,  265 => 178,  249 => 170,  233 => 161,  213 => 143,  203 => 139,  199 => 137,  188 => 132,  184 => 130,  182 => 129,  173 => 123,  165 => 118,  161 => 117,  157 => 116,  146 => 108,  142 => 107,  138 => 106,  134 => 105,  129 => 104,  127 => 103,  122 => 100,  118 => 98,  116 => 97,  112 => 94,  110 => 93,  106 => 90,  104 => 89,  100 => 86,  98 => 85,  94 => 82,  92 => 81,  88 => 78,  86 => 77,  82 => 74,  80 => 73,  76 => 70,  74 => 69,  69 => 66,  65 => 63,  63 => 62,  59 => 59,  57 => 58,  55 => 57,  51 => 55,  49 => 54,  46 => 53,);
    }

    public function getSource()
    {
        return "{#

/**
 * This file is part of IITBombayX-Drupal.
 *
 * IITBombayX-Drupal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * IITBombayX-Drupal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with IITBombayX-Drupal.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file
 * Default view template to display all the fields in a row.
 *
 * Available variables:
 * - view: The view in use.
 * - fields: A list of fields, each one contains:
 *   - content: The output of the field.
 *   - raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - class: The safe class ID to use.
 *   - handler: The Views field handler controlling this field.
 *   - inline: Whether or not the field should be inline.
 *   - wrapper_element: An HTML element for a wrapper.
 *   - wrapper_attributes: List of attributes for wrapper element.
 *   - separator: An optional separator that may appear before a field.
 *   - label: The field's label text.
 *   - label_element: An HTML element for a label wrapper.
 *   - label_attributes: List of attributes for label wrapper.
 *   - label_suffix: Colon after the label.
 *   - element_type: An HTML element for the field content.
 *   - element_attributes: List of attributes for HTML element for field content.
 *   - has_label_colon: A boolean indicating whether to display a colon after
 *     the label.
 *   - element_type: An HTML element for the field content.
 *   - element_attributes: List of attributes for HTML element for field content.
 * - row: The raw result from the query, with all data it fetched.
 *
 * @see template_preprocess_views_view_fields()
 *
 * @ingroup themeable
 */
#}

{% set fcount = 11 %}
{% for field in fields -%}

{% set fcount = fcount-1 %}
  {%- if field.wrapper_attributes.class[1] == 'views-field-field-course-link' %}
  {%  set course_link = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-course-image' %}
  {%  set image_link = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-course-code' %}  {%  set course_code = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-course-name' %}
  {%  set course_name = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-display-string'%}
  {%  set start_date = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-course-end'%}
  {%  set end_date = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-organisation' %}
  {%  set organisation = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-course-effort' %}
  {%  set effort = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-course-video' %}
  {%  set video = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-enrollment-end' %}
  {%  set enrollment_end = field.content  %}
  {%- endif %}

  {%- if field.wrapper_attributes.class[1] == 'views-field-field-enrollment-start' %}
  {%  set enrollment_start = field.content  %}
  {%- endif %}



  {% if(fcount == 0 ) %}
     <input type='hidden' value=\"{{ course_link }}\" id=\"course_id\"/>
     <input type='hidden' value=\"{{ edx_site_path }}\" id=\"edx_site_path\"/>
     <input type='hidden' value=\"{{ enrollment_start }}\" id=\"enrollment_start\"/>
     <input type='hidden' value=\"{{ enrollment_end }}\" id=\"enrollment_end\"/>
     <input type='hidden' value=\"{{ course_name }}\" id=\"course_name\"/>

      <section class=\"course-info\">
  \t<header class=\"course-profile\">
\t    <div class=\"intro-inner-wrapper\">
\t      <div class=\"table\">
\t      <section class=\"intro\">
        \t<div class=\"heading-group\">
\t          <h1> {{ site_path }}
        \t    {{ course_name }} 
\t            <a href=\"#\">{{ organisation }}</a>
        \t  </h1>
\t        </div>

        \t<div class=\"main-cta\">
        <a href=\"#\" class=\"register\">Enroll in {{ course_code }} </a><div id=\"register_error\"/> 
\t        </div>

      </section>


   {% if(video!='') %}
      <a class=\"media\" href=\"#video-modal\" rel=\"leanModal\">
\t<div class=\"hero\">
\t<img src=\" {{ edx_site_path }}{{ image_link  }}\" alt=\"{{ course_code }}\" />
        <div class=\"play-intro\"></div>
\t</div>
     </a>
   {% else %}
      <div class=\"media\">
\t<div class=\"hero\">
\t<img src=\" {{ edx_site_path }}{{ image_link  }}\" alt=\"{{ course_code }}\" />
        </div>
     </div>
   {% endif %}

    </div>
      </div>
  </header>

  <div class=\"container\">
    <div class=\"details\">

      <div class=\"inner-wrapper\" id=\"about_data\">
      </div>
    </div>

    <div class=\"course-sidebar\">
      <div class=\"course-summary\">
   <header>
    <div class=\"social-sharing\">
     <div class=\"sharing-message\">Share with friends and family!</div>
      
      <a href=\"http://twitter.com/intent/tweet?text=I+just+enrolled+in+{{ course_code }}{{course_name }}+through+https://twitter.com/iimbxonline:+{{ url(\"<front>\") }}{{ course_link }}/about\" class=\"share\">


       <span class=\"icon fa fa-twitter\" aria-hidden=\"true\"></span><span class=\"sr\">Tweet that you've enrolled in this course</span>
      </a>

      <a href=\"https://www.facebook.com/iimbxonline/\" class=\"share\">
        <span class=\"icon fa fa-thumbs-up\" aria-hidden=\"true\"></span><span class=\"sr\">Post a Facebook message to say you've enrolled in this course</span>
      </a>
      <a href=\"mailto:?subject=Take%20a%20course%20with%20IIMBX%20online&body=I%20just%20enrolled%20in%20{{ course_code}}%20{{ course_name }}%20through%20IIMBx%20{{ url(\"<front>\") }}{{ course_link }}/about\" class=\"share\">
        <span class=\"icon fa fa-envelope\" aria-hidden=\"true\"></span><span class=\"sr\">Email someone to say you've enrolled in this course</span>
      </a>
  </div>
</header>


        <ol class=\"important-dates\">
          {% if( course_code != '') %}         
<li class=\"important-dates-item\"><span class=\"icon fa fa-info-circle\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Course Number</p><span class=\"important-dates-item-text course-number\">{{ course_code }}</span></li>
          {% endif %}
          {% if (start_date !='') %}
            <li class=\"important-dates-item\"><span class=\"icon fa fa-calendar\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Classes Start</p><span class=\"important-dates-item-text start-date\">{{ start_date }}</span></li>
          {% endif %}
          {% if (end_date !='') %}
            <li class=\"important-dates-item\"><span class=\"icon fa fa-calendar\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Classes End</p><span class=\"important-dates-item-text start-date\">{{ end_date }}</span></li>
          {% endif %}
           {% if(effort !='') %}
            <li class=\"important-dates-item\"><span class=\"icon fa fa-pencil\" aria-hidden=\"true\"></span><p class=\"important-dates-item-title\">Estimated Effort</p><span class=\"important-dates-item-text effort\">{{ effort }}</span></li>
          {% endif %}    


        </ol>
    </div>

      <div class=\"coursetalk-read-reviews\">
          <div id=\"ct-custom-read-review-widget\" data-provider=\"iimbx\" data-course=\"IIMBx_{{ course_code }}\">

<iframe id=\"ct_widget_iframe\" class=\"ct-next-gen-widget\" src=\"https://www.coursetalk.com/widgets/v2.0/read/iframe-review-widget/nextgen/iimbx/IIMBx_{{ course_code }}\" scrolling=\"no\">
</iframe>
</div>
      </div>


  </div>

  </div>
</div>

  <div style=\"display:none;\">
    <form id=\"class_enroll_form\" method=\"post\" data-remote=\"true\" action=\"/change_enrollment\">
       <fieldset class=\"enroll_fieldset\">
         <legend class=\"sr\">Enroll</legend>
         <input name=\"course_id\" type=\"hidden\" value=\"{{ course_link }}\">
         <input name=\"enrollment_action\" type=\"hidden\" value=\"enroll\">
       </fieldset>
       <div class=\"submit\">
        <input name=\"submit\" type=\"submit\" value=\"enroll\">
       </div>
    </form>
  </div>

<div id=\"video-modal\" class=\"modal video-modal\">
<div class=\"inner-wrapper\">
{% if(video!='') %}
<iframe title=\"YouTube Video\" src=\"{{ video|replace({'watch?v=': \"embed/\"})}}\" allowfullscreen=\"\" width=\"560\" height=\"315\" frameborder=\"0\">
</iframe>
{#
<iframe title=\"YouTube Video\" src=\"{{ video}}\" allowfullscreen=\"\" width=\"560\" height=\"315\" frameborder=\"0\">
</iframe> #}
{% endif %}
</div>
</div>

<div id=\"lean_overlay\"></div>    
</div>
</section>
{% endif %}
{%- endfor %}



";
    }
}
