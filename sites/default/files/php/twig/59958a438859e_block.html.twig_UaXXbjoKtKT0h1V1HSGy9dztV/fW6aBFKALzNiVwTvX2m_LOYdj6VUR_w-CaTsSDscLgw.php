<?php

/* themes/custom/iimbx/templates/block.html.twig */
class __TwigTemplate_0dbdcb99ae14f71e1dece5fbb3120e4e4788a5d03e0ac0f3eb314077d2a63882 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee656cf1546057ff414eda923befa38776b7fb3dc0aee5f4107120a8ebadc760 = $this->env->getExtension("native_profiler");
        $__internal_ee656cf1546057ff414eda923befa38776b7fb3dc0aee5f4107120a8ebadc760->enter($__internal_ee656cf1546057ff414eda923befa38776b7fb3dc0aee5f4107120a8ebadc760_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/block.html.twig"));

        $tags = array("if" => 68, "block" => 72);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if', 'block'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 67
        echo "  ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title_prefix"]) ? $context["title_prefix"] : null), "html", null, true));
        echo "
  ";
        // line 68
        if ((isset($context["label"]) ? $context["label"] : null)) {
            // line 69
            echo "    <h2";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title_attributes"]) ? $context["title_attributes"] : null), "html", null, true));
            echo ">";
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true));
            echo "</h2>
  ";
        }
        // line 71
        echo "  ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["title_suffix"]) ? $context["title_suffix"] : null), "html", null, true));
        echo "
  ";
        // line 72
        $this->displayBlock('content', $context, $blocks);
        // line 77
        echo "
";
        
        $__internal_ee656cf1546057ff414eda923befa38776b7fb3dc0aee5f4107120a8ebadc760->leave($__internal_ee656cf1546057ff414eda923befa38776b7fb3dc0aee5f4107120a8ebadc760_prof);

    }

    // line 72
    public function block_content($context, array $blocks = array())
    {
        $__internal_5bee148b9eef1989b9960e91dad30c4a3ca720b71bfe89325f3cc3161f767294 = $this->env->getExtension("native_profiler");
        $__internal_5bee148b9eef1989b9960e91dad30c4a3ca720b71bfe89325f3cc3161f767294->enter($__internal_5bee148b9eef1989b9960e91dad30c4a3ca720b71bfe89325f3cc3161f767294_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 73
        echo "   
      ";
        // line 74
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["content"]) ? $context["content"] : null), "html", null, true));
        echo "
    
  ";
        
        $__internal_5bee148b9eef1989b9960e91dad30c4a3ca720b71bfe89325f3cc3161f767294->leave($__internal_5bee148b9eef1989b9960e91dad30c4a3ca720b71bfe89325f3cc3161f767294_prof);

    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 74,  83 => 73,  77 => 72,  69 => 77,  67 => 72,  62 => 71,  54 => 69,  52 => 68,  47 => 67,);
    }

    public function getSource()
    {
        return "{#
/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file is created for the display block.                        *
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
* Date: 19-07-2017                                                            *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date     By             Description                                 *
* --------------------------------------------------------------------------- *
* 1.0     19-jul-17  Mangesh G      Initial Version                             *
*                                                                             *
*                                                                             *
*******************************************************************************
 */


/**
 * @file
 * Default theme implementation to display a block.
 *
 * Available variables:
 * - plugin_id: The ID of the block implementation.
 * - label: The configured label of the block if visible.
 * - configuration: A list of the block's configuration values.
 *   - label: The configured label for the block.
 *   - label_display: The display settings for the label.
 *   - provider: The module or other provider that provided this block plugin.
 *   - Block plugin specific settings will also be stored here.
 * - content: The content of this block.
 * - attributes: array of HTML attributes populated by modules, intended to
 *   be added to the main container tag of this template.
 *   - id: A valid HTML ID and guaranteed unique.
 * - title_attributes: Same as attributes, except applied to the main title
 *   tag that appears in the template.
 * - content_attributes: Same as attributes, except applied to the main content
 *   tag that appears in the template.
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 *
 * @see template_preprocess_block()
 *
 * @ingroup themeable
 */
#}
  {{ title_prefix }}
  {% if label %}
    <h2{{ title_attributes }}>{{ label }}</h2>
  {% endif %}
  {{ title_suffix }}
  {% block content %}
   
      {{ content }}
    
  {% endblock %}

";
    }
}
