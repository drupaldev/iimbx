<?php

/* themes/custom/iimbx/templates/block--system-menu-block.html.twig */
class __TwigTemplate_9152c62a2c95125bf63980d3b5e4d2d16547071703c64a575b416ee5dd9fb9ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_782c48e17814a600321cef79cad0a175a5ab0970943fa26866eea6798cd8f6c9 = $this->env->getExtension("native_profiler");
        $__internal_782c48e17814a600321cef79cad0a175a5ab0970943fa26866eea6798cd8f6c9->enter($__internal_782c48e17814a600321cef79cad0a175a5ab0970943fa26866eea6798cd8f6c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/block--system-menu-block.html.twig"));

        $tags = array("block" => 72);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('block'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 69
        echo "
 
  ";
        // line 72
        echo "  ";
        $this->displayBlock('content', $context, $blocks);
        // line 75
        echo "
";
        
        $__internal_782c48e17814a600321cef79cad0a175a5ab0970943fa26866eea6798cd8f6c9->leave($__internal_782c48e17814a600321cef79cad0a175a5ab0970943fa26866eea6798cd8f6c9_prof);

    }

    // line 72
    public function block_content($context, array $blocks = array())
    {
        $__internal_a451062a38c8b4c52daea57283564563ce3ca4980296e551b8fd6bb56c046509 = $this->env->getExtension("native_profiler");
        $__internal_a451062a38c8b4c52daea57283564563ce3ca4980296e551b8fd6bb56c046509->enter($__internal_a451062a38c8b4c52daea57283564563ce3ca4980296e551b8fd6bb56c046509_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 73
        echo "    ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["content"]) ? $context["content"] : null), "html", null, true));
        echo "
  ";
        
        $__internal_a451062a38c8b4c52daea57283564563ce3ca4980296e551b8fd6bb56c046509->leave($__internal_a451062a38c8b4c52daea57283564563ce3ca4980296e551b8fd6bb56c046509_prof);

    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/block--system-menu-block.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  68 => 73,  62 => 72,  54 => 75,  51 => 72,  47 => 69,);
    }

    public function getSource()
    {
        return "{#
/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it 
* under the terms of the GNU General Public License as published by the Free 
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but 
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for 
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file is created for the display block for system menu.        *
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
* Date: 20-JUL-2017                                                            *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date     By             Description                                 *
* --------------------------------------------------------------------------- *
* 1.0     20-07-17  Mangesh G      Initial Version                             *
*                                                                             *
*                                                                             *
*******************************************************************************
 */


/**
 * @file
 * Theme override for a menu block.
 *
 * Available variables:
 * - plugin_id: The ID of the block implementation.
 * - label: The configured label of the block if visible.
 * - configuration: A list of the block's configuration values.
 *   - label: The configured label for the block.
 *   - label_display: The display settings for the label.
 *   - provider: The module or other provider that provided this block plugin.
 *   - Block plugin specific settings will also be stored here.
 * - content: The content of this block.
 * - attributes: HTML attributes for the containing element.
 *   - id: A valid HTML ID and guaranteed unique.
 * - title_attributes: HTML attributes for the title element.
 * - content_attributes: HTML attributes for the content element.
 * - title_prefix: Additional output populated by modules, intended to be
 *   displayed in front of the main title tag that appears in the template.
 * - title_suffix: Additional output populated by modules, intended to be
 *   displayed after the main title tag that appears in the template.
 *
 * Headings should be used on navigation menus that consistently appear on
 * multiple pages. When this menu block's label is configured to not be
 * displayed, it is automatically made invisible using the 'visually-hidden' CSS
 * class, which still keeps it visible for screen-readers and assistive
 * technology. Headings allow screen-reader and keyboard only users to navigate
 * to or skip the links.
 * See http://juicystudio.com/article/screen-readers-display-none.php and
 * http://www.w3.org/TR/WCAG-TECHS/H42.html for more information.
 */
#}

 
  {# Menu. #}
  {% block content %}
    {{ content }}
  {% endblock %}

";
    }
}
