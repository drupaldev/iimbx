<?php

/* themes/custom/iimbx/templates/container.html.twig */
class __TwigTemplate_e1c855f21d3da0b6a376f0a5bbacda6f88c61a05bc66f6ab28813a582a8c7e78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e9507c966b47bef58ab7be7b438a7d8a43dbe72e3293150601e2b5e1af597f85 = $this->env->getExtension("native_profiler");
        $__internal_e9507c966b47bef58ab7be7b438a7d8a43dbe72e3293150601e2b5e1af597f85->enter($__internal_e9507c966b47bef58ab7be7b438a7d8a43dbe72e3293150601e2b5e1af597f85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "themes/custom/iimbx/templates/container.html.twig"));

        $tags = array("set" => 58);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 58
        $context["classes"] = array(0 => ((        // line 59
(isset($context["has_parent"]) ? $context["has_parent"] : null)) ? ("js-form-wrapper") : ("")), 1 => ((        // line 60
(isset($context["has_parent"]) ? $context["has_parent"] : null)) ? ("form-wrapper") : ("")));
        // line 63
        echo "<div";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => (isset($context["classes"]) ? $context["classes"] : null)), "method"), "html", null, true));
        echo " stye=\"clear:unset;\">";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["children"]) ? $context["children"] : null), "html", null, true));
        echo "</div>
";
        
        $__internal_e9507c966b47bef58ab7be7b438a7d8a43dbe72e3293150601e2b5e1af597f85->leave($__internal_e9507c966b47bef58ab7be7b438a7d8a43dbe72e3293150601e2b5e1af597f85_prof);

    }

    public function getTemplateName()
    {
        return "themes/custom/iimbx/templates/container.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 63,  48 => 60,  47 => 59,  46 => 58,);
    }

    public function getSource()
    {
        return "{#
/**
* This file is part of IIMBX-Drupal.
*
* IIMBX-Drupal is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the Free
* Software Foundation, either version 3 of the License, or (at your option) any
* later version.
*
* IIMBX-Drupal is distributed in the hope that it will be useful,but
* WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
* more details.
*
* You should have received a copy of the GNU General Public License along with
* IIMBX-Drupal.  If not, see <http://www.gnu.org/licenses/>.

*******************************************************************************
*                                                                             *
* Purpose: This file is created for the display block for system menu.        *
*                                                                             *
* Created by: Mangesh Gharate                                                 *
*                                                                             *
* Date: 20-JUL-2017                                                           *
*                                                                             *
*                                                                             *
* Change Log:                                                                 *
* Version Date     By             Description                                 *
* --------------------------------------------------------------------------- *
* 1.0     20-07-17  Varun M       Initial Version                             *
*                                                                             *
*                                                                             *
*******************************************************************************
 */


/**
 * @file
 * Theme override of a container used to wrap child elements.
 *
 * Used for grouped form items. Can also be used as a theme wrapper for any
 * renderable element, to surround it with a <div> and HTML attributes.
 * See \\Drupal\\Core\\Render\\Element\\RenderElement for more
 * information on the #theme_wrappers render array property, and
 * \\Drupal\\Core\\Render\\Element\\container for usage of the container render
 * element.
 *
 * Available variables:
 * - attributes: HTML attributes for the containing element.
 * - children: The rendered child elements of the container.
 * - has_parent: A flag to indicate that the container has one or more parent
     containers.
 *
 * @see template_preprocess_container()
 */
#}
{%
  set classes = [
    has_parent ? 'js-form-wrapper',
    has_parent ? 'form-wrapper',
  ]
%}
<div{{ attributes.addClass(classes)}} stye=\"clear:unset;\">{{ children }}</div>
";
    }
}
